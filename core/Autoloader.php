<?php
    namespace core;
    
    abstract class Autoloader
    {
        /**
         * Ajoute à la pile d'autoload une classe
         */
        public static function autoload()
        {
            spl_autoload_register([__CLASS__, "chargeClass"]);
        }
        
        private static function chargeClass(string $class)
        {
            require $class.".php";
        }
        
    }
