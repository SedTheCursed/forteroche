<?php
    namespace app\views\frontend;
    
    use core\view\Form;
    
    class InscriptionView extends FrontView
    {
        use Form;
        
        protected function setTitre()
        {
            $this->titre = "S'inscrire";
        }
        
        protected function setContenu()
        {
            ob_start();
            ?>
            <h1>S'inscrire</h1>
            <form action="?page=newUser" method="post">
            
            <?php if (isset($_GET["erreur"])): ?>
            <p class="alert"><?= ucfirst($_GET["erreur"]); ?></p>
            <?php
            endif;
            
            echo $this->champ("login", "Votre identifiant", true);
            echo $this->champ("email", "Votre e-mail", false, "email");
            echo $this->champ("password", "Votre mot de passe", false, "password");
            echo $this->champ("confirmation", "Confirmer votre mot de passe", false, "password");
            ?>	
            	<p id="pwdDissemblables">Les mots de passe ne correspondent pas.</p>
            	<p>
                	<input type="submit" value="S'inscrire" />
                	<input type="reset" value="Annuler"/>
            	</p>
            </form>
            <?php
            $this->contenu = ob_get_clean();
        }
        
        protected function setScript()
        {
            $this->script = "<script src='scripts/inscription.js'></script>";
        }
    }
