<?php
namespace core\entities;

abstract class Entity
{
    public function __construct(array $donnees)
    {
        $this->hydrate($donnees);
    }
    
    protected function hydrate(array $donnees)
    {
        foreach ($donnees as $info => $value) {
            $method = "set".ucfirst($info);
            
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
}
