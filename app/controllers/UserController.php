<?php
    namespace app\controllers;
    
    use app\models\UsersManager;
    use app\models\CommentairesManager;
                    
    class UserController extends Controller
    {
        /**
         * Login de l'utilisateur
         * @var string
         */
        private $login;
        
        /**
         * Mot de passe de l'utilisateur
         * @var string
         */
        private $password;
        
        /**
         * Nom de l'utilisateur sur le site
         * @var string
         */
        private $pseudo;
        
        /**
         * Tableau contenant le nom de la photo de l'utilisateur
         * et son emplacement temporaire
         * @var array
         */
        private $avatar;
        
        /**
         * Email de l'utilisateur
         * @var string
         */
        private $email;
        
        /**
         * Statut d'administrateur
         * @var bool
         */
        private $admin;
        
        /**
         * @var CommentairesManager
         */
        private $commentsManager;
        
        /**
         * Partie front ou back office d'où est appellée la classe
         * @var string
         */
        private $side;
        
        public function __construct(
            UsersManager $manager,
            array $donnees,
            string $action,
            string $retour,
            array $avatar = null,
            CommentairesManager $commentsManager = null)
        {            
            if (isset($donnees["id"])) {
                $this->setId($donnees["id"]);
            }
            
            if ($avatar !== null) {
                $this->setAvatar($avatar);
            }
            
            parent::__construct($manager, $donnees, $action, $retour);
            
            if($commentsManager !== null) {
                $this->setCommentsManager($commentsManager);
            }
        }
        
        /**
         * @see \app\controllers\Controller::add()
         */
        protected function add()
        {
            try {
                $url = $this->retour."&success=add";
                $admin = ($this->admin === null)? false: $this->admin;
                
                $this->manager->addNew(
                    $this->login,
                    $this->email,
                    $this->password,
                    $this->pseudo,
                    $admin);
                
                if (!isset($_SESSION["user"])) {
                    $id = $this->manager->getDB()->lastInsertId();
                    $_SESSION["user"] = $this->manager->one($id);
                }
                
                header("location:".$url);
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage());
            }
        }

        /**
         * @see \app\controllers\Controller::delete()
         * La suppression d'un utilisateurs entrainent celle des commentaires
         * et on remplace l'auteur par un utilisateur fantôme pour ceux qui ne le sont pas
         */
        protected function delete()
        {
            try {
                $url = $this->retour."&success=update";
                $users = $this->manager;
                $comments = $this->commentsManager;
                $id = $this->id;
                $pseudo = $users->one($id)->getPseudo();
                $avatar = $users->one($id)->getAvatar();
                
                $userComments = $comments->postedByUser($id);
                
                if (!empty($userComments)) {
                    $comments->fromDeletedUser($id);
                    foreach ($userComments as $comment) {
                        $comments->manualDelete($comment);
                    }
                }
                
                $comments->toDeletedUser($pseudo);
                unlink($avatar);
                $users->delete($id);
                header("location:".$url);
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage());
            }
        }
        
        /**
         *  SETTERS
         */
        
        protected function setLogin(string $login)
        {
            $login= strip_tags(strtolower($login));
            
            if ($this->action === "update") {
                $identique = ($login === $this->manager->one($this->id)->getLogin());
                $disponible = $this->manager->loginDisponible($login) | $identique;
            } else {
                $disponible = $this->manager->loginDisponible($login);
            }

            if ($disponible) {
                $this->login = $login;
            } else {
                throw new \Exception("Login indisponible");
            }
        }
    
        protected function setPassword(string $password)
        {
            if (!empty($password)) {
                $this->password = password_hash($_POST["password"], PASSWORD_DEFAULT);
            }
        }
    
        protected function setPseudo(string $pseudo)
        {
            if ($this->action === "update") {
                $identique = ($pseudo === $this->manager->one($this->id)->getPseudo());
                $disponible = ($this->manager->pseudoDisponible($pseudo) && $pseudo !== "Anonyme") | $identique;
            } else {
                $disponible = $this->manager->pseudoDisponible($pseudo) && $pseudo !== "Anonyme";
            }
            
            if ($disponible) {
                $this->pseudo = strip_tags($pseudo);
            } else {
                throw new \Exception("Ce pseudo est déja pris");
            }
        }
    
        protected function setAvatar(array $avatar)
        {
            if ($avatar["size"]!==0 && $avatar !== null) {
                $valide = ($avatar["error"] == 0);
                $taille = ($avatar["size"] <= 50*1000);
                $extension_autorisees = ["image/jpeg","image/gif","image/png"];
                $image = in_array(mime_content_type($avatar["tmp_name"]), $extension_autorisees);
                
                if ($valide){
                    if($taille) {
                        if($image) {
                            $extension=pathinfo($avatar["name"])["extension"];
                            $fileName = $this->id.".".$extension;
                            
                            $this->avatar = [
                                "fileName"      =>  $fileName,
                                "temporaryName" =>  $avatar["tmp_name"]
                            ];
                        } else {
                            throw new \Exception("Le fichier n'est pas une image.");
                        }
                    } else {
                        throw new \Exception("Le fichier est trop volumineux");
                    }
                } else {
                    throw new \Exception("Il y a eu une erreur dans l'envoi du fichier.");
                }
            }
        }
    
        protected function setEmail(string $email)
        {
            $email=strtolower($email);
            if ($this->action === "update") {
                $identique = ($email === $this->manager->one($this->id)->getEmail());
                $disponible = $this->manager->mailDisponible($email) | $identique;
            } else {
                $disponible = $this->manager->mailDisponible($email);
            }
            
            if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}(\.[a-z]{2,4}){1,2}$#", $email)) {
                if ($disponible) {
                    $this->email = strip_tags($email);
                } else {
                    throw new \Exception("Cette adresse est déjà utilisée par un autre utilisateur");
                }
            } else {
                throw new \Exception("L'E-mail fourni n'est pas valide.");
            }
        }
    
        protected function setAdmin(string $admin)
        {
            $this->admin = ($admin==="oui")?true:false;
        }
        
        protected function setCommentsManager(CommentairesManager $commentsManager)
        {
            $this->commentsManager = $commentsManager;
        }
        
        protected function setUpdateCommands() {
            ob_start();
            $id         =   $this->id;
            $login      =   $this->login;
            $pseudo     =   $this->pseudo;
            $email      =   $this->email;
            $admin      =   $this->admin;
            $password   =   $this->password;
            $avatar     =   $this->avatar;
            
            if ($login !== null) {
                $this->manager->updateLogin($login, $id);
            }
            
            if ($pseudo !== null) {
                $this->manager->updatePseudo($pseudo, $id);
            }
            
            if ($email !== null) {                
                $this->manager->updateEmail($email, $id);
            }
             
            if ($admin !== null) {
                $this->manager->updateAdmin($admin, $id);
            }
            
            if ($password !== null) {
                $this->manager->updatePassword($password, $id);
            }
            
            if ($avatar !== null) {
                $this->manager->updateAvatar(
                    $this->avatar["fileName"],
                    $this->avatar["temporaryName"],
                    $id);
            }
            
            if ($this->side === "front") {
                $_SESSION["user"]=$this->manager->one($id);
            }
            
            $this->updateCommands = ob_get_clean();            
        }
        
        protected function setSide(string $side)
        {
            $this->side = $side;
        }
    }
