<?php
    namespace app\views\elements;
    
    trait Commandes
    {
        protected function layout(bool $first, bool $last, array $url):string
        {
            ob_start();
            ?>
        	<div class="commande">
                <div>
            		<?php if (!$first): ?>
                    <a title="premier" href="<?= $url["first"]; ?>">
                    <?php endif; ?>
            		<span class="glyphicon glyphicon-fast-backward"></span>
            		<?php if (!$first): ?>
                    </a>
                    <a title="précédent" href="<?= $url["previous"]; ?>">
                    <?php endif; ?>
            		<i class="glyphicon glyphicon-backward"></i>
            		<?php if (!$first): ?>
                    </a>
                    <?php endif; ?>
            	</div>
            	<div>
            		<?php if (!$last) :?>
            		<a title="suivant" href="<?= $url["next"]; ?>">
                    <?php endif; ?>
                    <i class="glyphicon glyphicon-forward"></i>
            		<?php if (!$last): ?>
                    </a>
                    <a title="dernier" href="<?= $url["last"]; ?>">
                    <?php endif; ?>
            		<i class="glyphicon glyphicon-fast-forward"></i>
            		<?php if (!$last): ?>
                    </a>
                    <?php endif; ?>
            	</div>
            </div>
            <?php
            
            return ob_get_clean();
        }
    }
