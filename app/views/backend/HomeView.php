<?php
    namespace app\views\backend;
    
    use app\entities\Commentaire;
    use app\views\backend\templates\TextView;
                                    
    class HomeView extends TextView
    {   
        /**
         * Le dernier commentaire posté sur le site
         * @var Commentaire
         */
        private $lastComment;
    
        public function __construct(Commentaire $lastComment)
        {
            $this->setLastComment($lastComment);
            parent::__construct();
        }
        
        private function setLastComment(Commentaire $comment)
        {
            $this->lastComment = $comment;
        }
        
        protected function setTitre()
        {
            $this->titre = "Accueil";
        }
        
        protected function setContenu()
        {
            $commentaire = $this->lastComment;
            
            ob_start();
            ?>
            <h1>Bienvenue <?= $_SESSION["user"]->getPseudo(); ?> !</h1>
            <div id="lastComment">
            	<h3>Dernier commentaire&nbsp;:</h3>
            	<p><strong class="auteur"><?= $commentaire->getAuteur(); ?></strong> - <?php
                echo $commentaire->getDateAjout();
                if ($commentaire->getDateModif() !== null) {
                    echo " ".$commentaire->getDateModif();
                } ?></p>
                <?= $commentaire->getContenu(); ?>
            </div>
            <?php
            echo $this->textForm("Billet", false);
            $this->contenu =  ob_get_clean();
        }
    }
