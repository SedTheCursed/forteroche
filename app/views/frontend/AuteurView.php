<?php
    namespace app\views\frontend;
    
    class AuteurView extends FrontView
    {
        protected function setTitre()
        {
            $this->titre = "A propos de Jean Forteroche";
        }
        
        protected function setContenu()
        {
            ob_start();
            ?>
        	<h1>A propos de l'auteur</h1>
        	<div  class="auteur">
        		<img id="auteur" src="img/jeanforteroche.jpg" alt="Jean Forteroche"/>
        		<p>
        			JEAN FORTEROCHE est né le 12 juillet 1972 à Lyon. Et depuis, il improvise, essentiellement sur les planches de théâtre. Pris du virus de la comédie depuis son plus jeune âge, il devient comédien au terme d'une scolarité assez moyenne, où il enchaîne les oeuvres modernes et classiques, jusqu'à un Scapin aux théâtre des Celectins, qui lui ouvre les portes de la télévision et du cinéma.<br/>
        			Même s'il y est souvent cantonné à des second rôles, ce milieu lui donne l'occasion de forger ses permière armes d'auteur avec le scénario du téléfilm "Les Geraniums d'un jour de pluie," rapidement suivi par la piece "Dix bonnes raisons de mettre votre chat dans une lessiveuse." Depuis, il s'est essayé à de nombreux genres litteraires&nbsp;: poèsie, nouvelles, vaudeville et même science-fiction. 
        		</p>
        		<p>
        			Dernièrement, lorsqu'il n'est pas accaparé par ses fonctions de showrunner de la serie <strong>Carl Na</strong>, adapté de ses romans, on le retrouve derrière une Remington* et jamais loin de ses Dionées attrape-mouches apprivoisées, affectueusement appellées Audrey et Audrey II.
        		</p>
        		<p id="note">* En verité, il utilise un ordinateur, qu'il a surnommé Remington.</p>
        		<h2>Bibliographie Sélective</h2>
        		<div class="bibliographie">
        			<ul>
        				<li>Les Geraniums d'un jour de pluie (Scénario, 2000)</li>
        				<li>
        					<strong>Triptyque des animaux ménagers (Théâtre)</strong>
        					<ol>
        						<li>Dix bonnes raisons de mettre votre chat dans une lessiveuse (2000)</li>
        						<li>Et votre chien dans un lave-vaiselle (2005)</li>
        						<li>Au secours&nbsp;! Mes animaux veulent m'enfourner&nbsp;! (2010)</li>
        					</ol>
        				</li>
        				<li>Tous les chiens mènent au rhum (Roman, 2002)</li>
        				<li>Le dragon dans la chaudière (Nouvelles, 2004)</li>
        				<li>
        					<strong>Les chroniques de Na-guerre (Romans)</strong>
        					<ol>
        						<li>Le manifeste des parties qu'on mutile (2006)</li>
        						<li>L'ultra-national (2008)</li>
        						<li>Les lendemains qui décantent (2010)</li>
        					</ol>
        				</li>
        				<li>L'odieux adieu aux dieux (Nouvelles, 2011)</li>
        				<li>Rêveries et cauchemars (Poésie, 2011)</li>
        				<li>Mon chat, mon chien et moi (Scénario, 20014)</li>
        				<li>
        					<strong>Carl Na (Scénarii, 2016)</strong>
        					<ol>
        						<li>Pilote</li>
        						<li>Engueuler Mars</li>
        						<li>Le cas Pital</li>
        					</ol>
        				</li>
        				<li>Billet simple pour l'Alaska (Roman, en cours d'écriture)</li>
        			</ul>
        		</div>
        	</div>
        	<?php
            $this->contenu = ob_get_clean();
        }
    }
