$(function(){
	$("#password").on("change",function(e){
		if ($(e.target).val()!=="") {
			$("#confirmation").prop("required",true);
		} else {
			$("#confirmation").prop("required",false);
		}
	});
	
	$("#confirmation").on("blur",function(e){
		var password = $("#password").val(),
			confirmation = $(e.target).val();
		
		if(password !== confirmation) {
			$("input[type=submit]").attr("disabled","disabled");
			$("#pwdDissemblables").show();
		} else if ($("input[type=submit]").prop("disabled")) {
			$("input[type=submit]").prop("disabled", false);
			$("#pwdDissemblables").hide();
		}
	});
});