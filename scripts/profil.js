$(function(){
	$("button.modifInfo").each(function() {
		$(this).on("click",function(e){
			e.preventDefault();
			
			var button = $(e.target);
			
			button.prev().children(".info").hide();
			button.prev().children(".modifier").show();
			button.hide();
			button.next().show();
			$(".success").hide();
			$(".alert").hide();
			
		});
	});
	
	$("button.annuler").each(function(){
		$(this).on("click",function(e){
			e.preventDefault();
			
			var button = $(e.target).parent();
			
			button.prev().prev().children(".info").show();
			button.prev().prev().children(".modifier").hide();
			button.prev().show();
			button.hide();
		});
	});
	
	$("input[value=Supprimer]").on("click",function(e){
		var confirmation = confirm("Voulez-vous vraiment supprimer votre compte ?\n(Cette action est irréversible.)"),
			checked = $("input[name=deleteAccount").prop("checked");
		
		if (!confirmation | !checked) {
			e.preventDefault();
		}
	});
});