<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\ModifierView;
    use app\entities\Commentaire;
                    
    class ModifierCommentaireView extends ModifierView
    {
        public function __construct(Commentaire $comment, string $retour)
        {
            parent::__construct($comment, $retour);
        }
        
        protected function setTitre()
        {
            $this->titre = "Modifier le commentaire";
        }
        
        protected function setContenu()
        {
            $contenu = $this->item->getContenu();
            
            ob_start();
            echo $this->textForm("Commentaire", true, false, null, $contenu, false);
            ?>
           	<div><a href="<?= $this->retour; ?>"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a></div>
            <?php
            $this->contenu = ob_get_clean();
        }
    }
