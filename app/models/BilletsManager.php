<?php
    namespace app\models;
    
    use app\entities\Billet;
    use core\models\Manager;
                
    class BilletsManager extends Manager
    {
        /**
         * Id du premier billet
         * @var int
         */
        private $firstId;
        /**
         * Id du derneir billet
         * @var int
         */
        private $lastId;
        
        public function __construct(\PDO $database)
        {
            parent::__construct($database);
            $this->setFirstId();
            $this->setLastId();
        }
        
        public function create(string $titre, string $contenu)
        {
            $request="INSERT INTO billets( bill_titre, bill_contenu, bill_date_ajout) VALUES (:titre, :contenu, NOW())";
            $donnees=["titre" => $titre, "contenu" => $contenu];
            
            $this->manage($request, $donnees);
        }
            
        public function one(int $id):Billet
        {
            $request="SELECT
                        bill_id AS id, 
                        bill_titre AS titre,
                        bill_contenu AS contenu,
                        DATE_FORMAT(bill_date_ajout,'Le %d/%m/%Y à %H:%i') AS dateAjout,
                        DATE_FORMAT(bill_date_modif,'(modifié le %d/%m/%Y à %H:%i)') AS dateModif
                     FROM billets WHERE bill_id = ?";
            $settings=[$id];
            
            $donnees = $this->query($request,$settings);
            
            if(!empty($donnees)) {
                return new Billet($donnees[0], $this->firstId, $this->lastId);
            } else {
               throw new \Exception("erreur");
            }
        }
        
        public function last():Billet
        {
           return $this->one($this->lastId); 
        }
            
        public function all():array
        {
            $billets = [];
            $request = "SELECT 
                            bill_id AS id, 
                            bill_titre AS titre,
                            DATE_FORMAT(bill_date_ajout,'%d/%m/%Y') AS dateAjout
                        FROM billets
                        ORDER BY bill_id";
            
            $donnees= $this->query($request);
            
            foreach ($donnees as $properties) {
                $billets[] = new Billet($properties, $this->firstId, $this->lastId);
            }
            
            return $billets; 
        }
        
        /**
         * Recuperation par 20 pour paginer la liste des billets dans l'interface de gestion
         */
         public function paginated(int $page):array
        {
            $billets = [];
            $request ="SELECT
                            bill_id AS id, 
                            bill_titre AS titre,
                            DATE_FORMAT(bill_date_ajout,'%d/%m/%Y') AS dateAjout
                        FROM billets
                        ORDER BY bill_id DESC
                        LIMIT :firstArticle, 20";
            $bindings = [
                "firstArticle" => $page*20
            ];
            
            $donnees = $this->queryWithIntegral($request, $bindings);
            
            foreach ($donnees as $properties) {
                $billets[] = new Billet($properties, $this->firstId, $this->lastId);
            }
            
            return $billets;
        }
        
        public function totalPages():int
        {
            $request="SELECT COUNT(bill_id) AS nbBillet FROM billets";
            
            return (int) floor($this->query($request)[0]["nbBillet"]/20);
        }
        
        public function update(string $titre, string $contenu, int $id)
        {
            $request = "UPDATE billets
                        SET bill_titre= :titre,
                            bill_contenu= :contenu,
                            bill_date_modif=NOW()
                        WHERE bill_id= :id";
            $donnees = [
              "titre"   =>  $titre,
              "contenu" =>  $contenu,
              "id"      =>  $id
            ];
            
            $this->manage($request, $donnees);
        }
        
        public function delete(int $id)
        {
            $request = "DELETE FROM billets WHERE bill_id=?";
            $donnees = [$id];
            $this->manage($request, $donnees);
        }
        
        /**
         * GETTERS & SETTERS
         */
        
        public function getFirstId():int
        {
            return $this->firstId;
        }
        
        public function getLastId():int
        {
            return $this->lastId;
        }
    
        private function setFirstId()
        {
            $request = "SELECT MIN(bill_id) AS id FROM billets";
            $reponse = $this->query($request);
            $this->firstId = $reponse[0]["id"];
        }

        private function setLastId()
        {
            $request = "SELECT MAX(bill_id) AS id FROM billets";
            $reponse = $this->query($request);
            $this->lastId = $reponse[0]["id"];
        }
    }

