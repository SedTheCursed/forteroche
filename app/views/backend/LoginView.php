<?php
    namespace app\views\backend;

    use app\views\backend\templates\BackView;
    use app\views\elements\Login;
                                
    class LoginView extends BackView
    {
        use Login;
    }
