<?php
    namespace app\entities;
    
    use \core\entities\Entity;
    
    abstract class Poste extends Entity
    {
        /**
         * Id unique du texte
         * @var int
         */
        protected $id;
        
        /**
         * Titre du texte
         * @var string
         */
        protected $titre;
        
        /**
         * Contenu du texte
         * @var string
         */
        protected $contenu;
        
        /**
         * Date de création du texte
         * @var string
         */
        protected $dateAjout;
        
        /**
         * Date de la dernière modification du texte.
         * @var string
         */
        protected $dateModif;
        
        /**
         *  GETTERS
         */
        
        public function getId():int
        {
            return $this->id;
        }
        
        public function getTitre():string
        {
            return $this->titre;
        }
        
        public function getContenu():string
        {
            return $this->contenu;
        }
        
        public function getDateAjout():string
        {
            return $this->dateAjout;
        }
        
        public function getDateModif()
        {
            return $this->dateModif;
        }
        
        /**
         *  SETTERS
         */
        
        public function setId(int $id)
        {
            $this->id = $id;
        }
        
        public function setTitre(string $titre)
        {
            $this->titre = $titre;
        }
        
        public function setContenu(string $contenu)
        {
            $this->contenu = $contenu;
        }
        
        public function setDateAjout(string $dateAjout)
        {
            $this->dateAjout = $dateAjout;
        }
        
        public function setDateModif($dateModif)
        {
            if(!is_null($dateModif)) {
                $this->dateModif = $dateModif;
            }
        }
    }

