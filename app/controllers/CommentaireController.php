<?php
    namespace app\controllers;
    
    use app\models\CommentairesManager;
                
    class CommentaireController extends Controller
    {
        /**
         * Contenu du commentaire
         * @var string
         */
        private $contenu;
        
        /**
         * Id de l'auteur
         * @var int
         */
        private $auteur;
        
        /**
         * Id de l'eventuel commentaire auxquel le commentaire répond 
         * @var int
         */
        private $reponse;
        
        /**
         * Id de l'article auxquel appartient le commentaire
         * @var int
         */
        private $article;
        
        public function __construct(CommentairesManager $manager, array $donnees, string $action, string $retour)
        {
            parent::__construct($manager, $donnees, $action, $retour);
        }
        
        /**
         * @see \app\controllers\Controller::add()
         */
        protected function add()
        {
            try {
                $url = $this->retour."&success=add";
                
                $this->manager->create($this->contenu, $this->auteur, $this->article, $this->reponse);
                header("location:".$url."#commentaires");
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage()."#commentaires");
            }
        }
        
        /**
         * @see \app\controllers\Controller::delete()
         */
        protected function delete()
        {
            try {
                $this->manager->manualDelete($this->id);
                
                header("location:".$this->retour);
                
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage());
            }
        }
        
        
        /**
         *  SETTERS
         */
        
        protected function setContenu(string $contenu)
        {
            $this->contenu = $contenu;
        }
        
        protected function setAuteur(int $auteur)
        {
            $this->auteur = $auteur;
        }
        
        protected function setReponse(int $reponse)
        {
            $this->reponse = $reponse;
        }
        
        protected function setArticle(int $article)
        {
            $this->article = $article;
        }
        
        /**
         * @see \app\controllers\Controller::setUpdateCommands()
         */
        protected function setUpdateCommands()
        {
            ob_start();
            $this->manager->updateContent($this->id, $this->contenu);
            $this->updateCommands = ob_get_clean();
        }
    }
