<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\ManageView;
    
    class ManageCommentairesView extends ManageView
    {
        protected function commandes(string $items = "Commentaires"):string
        {
            return parent::commandes($items);
        }
        
        protected function setTitre()
        {
            $this->titre = "Commentaires";
        }
        
        protected function setContenu()
        {
            $titre = "Commentaires";
            $head = ["Date","Article", "Message"];
            $body = ["getDateAjout", "getArticleTitre", "getContenu"];
            $entity = "Commentaire";
            
            $this->contenu = $this->affichage($titre, $head, $body, $entity, true);
        }
    }
