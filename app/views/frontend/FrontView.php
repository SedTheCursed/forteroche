<?php
namespace app\views\frontend;

use app\views\elements\View;

abstract class FrontView extends View
{
    public function view(string $template="/templates/frontend.php")
    {
        parent::view($template);
    }
}
