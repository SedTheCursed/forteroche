<?php
    namespace app\controllers;
    
    use app\models\UsersManager;
    use app\entities\User;
                                
    class Connexion
    {
        /**
         * @var UsersManager
         */
        private $manager;
        
        /**
         * Determine si on se connecte sur le front ou le backoffice
         * @var bool
         */
        private $frontend;
        
        
        /**
         * Adresse de la page d'accueil
         * @var string
         */
        private $accueil;
        
        /**
         * Adresse de la page où retourner après la connexion
         * @var string
         */
        private $retour;
                
        public function __construct(UsersManager $manager, bool $frontend = true)
        {
            $this->setManager($manager);
            $this->setFrontend($frontend);
            $this->setAccueil($frontend);
            $this->setRetour($frontend);
        }
        
        /**
         * Execute le processus de connexion
         * 
         * @throws \core\exceptions\AdminException
         * @throws \core\exceptions\LoginException
         */
        public function exec()
        {
            if (isset($_POST["login"])
                && !empty($_POST["login"])
                && isset($_POST["password"])
                && !empty($_POST["password"])) {
                    $login = strtolower($_POST["login"]);
                    $password = $_POST["password"];
                } else {
                    header($this->accueil."?page=login&erreur=".urlencode("les informations fournies sont incomplètes"));
                }
            
            try {
                $user = $this->manager->connexion($login);
                
                if($user && password_verify($password, $user->getPassword())) {
                    if ($this->frontend) {
                        $this->successfulConnexion($login, $user->getPassword(), $user);
                    } else {
                        if ($user->getAdmin()) {
                            $this->successfulConnexion($login, $user->getPassword(), $user);
                        } else {
                            throw new \core\exceptions\AdminException();
                        }
                    }
                } else {
                    throw new \core\exceptions\LoginException();
                }
            } catch (\Exception $e) {
                header("location:$this->accueil?page=login&erreur=".urlencode($e->getMessage()));
            }
        }
        
        /**
         * Procede la connexion
         */
        private function successfulConnexion(string $login, string $password, User $user)
        {
            if (isset($_POST["stayCo"])) {
                setcookie("login", $login, time()+3600*24*30, null, null, false, true);
                setcookie("password", $password, time()+3600*24*30, null, null, false, true);
            }
            
            $_SESSION["user"]=$user;
            
            header("location:".$this->retour);
        }
        
        /**
         *  SETTERS
         */
        
        private function setManager(UsersManager $manager)
        {
            $this->manager = $manager;
        }
        
        private function setFrontend(bool $frontend)
        {
            $this->frontend = $frontend;
        }
        private function setAccueil(bool $frontend)
        {
            $this->accueil = ($frontend)?"index.php":"admin.php";
        }
        
        private function setRetour(bool $frontend)
        {
            if ($frontend) {
                if (isset($_SESSION["retour"])) {
                    $this->retour = $_SESSION["retour"];
                } else {
                    $this->retour = "index.php";
                }
            } else {
                $this->retour = "admin.php";
            }
        }
    }
