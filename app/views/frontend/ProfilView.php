<?php
    namespace app\views\frontend;
    
    use app\entities\User;
    use core\view\Form;
    
    class ProfilView extends FrontView
    {    
        use Form;
        
        /**
         * Utilisateur à modifier
         * @var User
         */
        private $user;
        /**
         * Bloc des boutons de modification
         * @var string
         */
        private $button;
        /**
         * Bloc de boutons de modification de l'avatar
         * @var string
         */
        private $buttonImg;
        
        public function __construct($user)
        {
            $this->setUser($user);
            $this->setButton();
            $this->setButtonImg();
            parent::__construct();   
        }
        
        public function getUser():User
        {
            return $this->user;
        }
        
        private function setUser(User $user)
        {
            $this->user= $user;
        }
        
        private function setButton()
        {
            ob_start();
            ?>
        		<button class="modifInfo">Modifier</button>
        		<div class="modifier"><input type="submit" value="Modifier"/><button class='annuler'>Annuler</button></div>
            <?php
            $this->button = ob_get_clean();
        }
        
        private function setButtonImg()
        {
            ob_start();
            ?>
        		<button class="modifInfo">Modifier</button>
        		<div class="modifier">
        			<input type="submit" value="Modifier"/><br/>
        			<button class='annuler'>Annuler</button>
    			</div>
            <?php
            $this->buttonImg = ob_get_clean();
        }
        
        protected function setTitre()
        {
            $this->titre = "Profil de ".$this->user->getPseudo();
        }
        
        protected function setContenu()
        {
            ob_start();
            ?>
            <h1>Profil de <?= $this->user->getPseudo(); ?></h1>
            <form class="profil" action="?page=updateUser" method="post">
            	<?php if (isset($_GET["erreur"])): ?>
                    <p class="alert"><?= ucfirst($_GET["erreur"]); ?></p>
                <?php elseif (isset($_GET["success"]) && $_GET["success"]==="password"): ?>
    				<p class='success'>Votre mot de passe a bien été mis à jour.</p>
                <?php endif; ?>
            	<div><div><label>Login&nbsp;:</label> <span class="info"><?= $this->user->getLogin();?></span></div></div>
        		<div>
        			<div>
        				<label>Pseudo&nbsp;:</label>
        				<span class="info"><?= $this->user->getPseudo();?></span>
        				<input class="modifier" type="text" name="pseudo" value="<?= $this->user->getPseudo();?>" />
    				</div>
        			<?= $this->button; ?>
        		</div>
    		</form>
    		<form class="profil" action="?page=updateUser" method="post">
        		<div>
        			<div>
        				<label>Email&nbsp;:</label> 
        				<span class="info"><?= $this->user->getEmail(); ?></span>
        				<input class="modifier" type="email" name="email" value="<?= $this->user->getEmail(); ?>"/>
    				</div>
        			<?= $this->button; ?>
        		</div>
    		</form>
    		<form class="profil" action="?page=updateUser" method="post" enctype="multipart/form-data">
        		<div>
        			<div>
    					<label>Avatar&nbsp;:</label> 
        				<img class="avatar" src="<?= $this->user->getAvatar();?>" alt="avatar de <?= $this->user->getPseudo(); ?>"/>
        				<input class="modifier" type="file" name="avatar" id="avatar" required />
    				</div>
        			<?= $this->buttonImg; ?>
        		</div>
    		</form>
    		<form class="profil" action="?page=updateUser" method="post">
        		<div>
        			<div>
        				<label>Mot de passe</label>
        				
        				<div class="modifier" >
            				<?php
            				echo $this->champ("password", "", false, "password");
            				echo $this->champ("confirmation", "Veuillez confirmer le nouveau mot de passe", false, "password");
            				?>
            				<p id="pwdDissemblables">Les mots de passe ne correspondent pas.</p>	
            			</div>
    				</div>
        			<?= $this->button; ?>
        		</div>
            </form>
            <form action="?page=deleteUser" method="post">
            	<hr/>
            	<p>
            		<input type="hidden" name="id" value="<?= $this->user->getId(); ?>" />
            		<input type="checkbox" name="deleteAccount" id="deleteAccount" />
            		<label for="deleteAccount">Je desire supprimer mon compte.</label><br/>
            		<input type="submit" value="Supprimer" />
            	</p>
            </form>
            <?php
            $this->contenu = ob_get_clean();
        }
        
        protected function setScript()
        {
            $this->script = "<script src='scripts/profil.js'></script>
            <script src='scripts/inscription.js'></script>";
        }
    }  
