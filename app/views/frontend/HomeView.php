<?php
    namespace app\views\frontend;
    
    use app\entities\Billet;
    use app\entities\Edito;
                                
    class HomeView extends FrontView
    {
        /**
         * Dernier billet publié
         * @var Billet
         */
        private $billet;
        
        /**
         * Dernier Edito publié
         * @var Edito
         */
        private $edito;
        
        public function __construct(Billet $billet, Edito $edito)
        {
            $this->setBillet($billet);
            $this->setEdito($edito);
            parent::__construct();
        }
            
        private function setBillet(Billet $billet)
        {
            $this->billet = $billet;
        }
    
        private function setEdito(Edito $edito)
        {
            $this->edito = $edito;
        }
    
        protected function setTitre()
        {
            $this->titre = "Accueil";
        }
        
        protected function setContenu()
        {
            ob_start();
            ?>
    				<a title="Lire la suite" href="?page=billet&amp;id=<?= $this->billet->getId(); ?>">
        				<div class="preview">
        					<h2><?= $this->billet->getTitre(); ?></h2>
        					<p><?= $this->billet->getPreview(); ?>... <strong>Lire la suite.</strong></p>
        				</div>
    				</a>
    				<h1>
    					<?= $this->edito->getTitre(); ?><br/>
    					<span class="date-humeur"><?= $this->edito->getDateAjout() ?></span>
    				</h1>
    				<div class="humeur">
    					<?= $this->edito->getContenu(); ?>
    					<p id="signature">Jean Forteroche</p>
    				</div>    
			<?php
            $this->contenu = ob_get_clean();
        }
    }
