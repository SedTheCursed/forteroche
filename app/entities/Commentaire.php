<?php
    namespace app\entities;
    
    class Commentaire extends Poste
    {
        /**
         * Pseudo de l'auteur du commentaire
         * @var string
         */
        private $auteur;
        
        /**
         * Fichier de l'avatar de l'auteur
         * @var string
         */
        private $avatar;
        
        /**
         * Id de l'article auquel répond le commentaire
         * @var int
         */
        private $article;
        
        /**
         * Titre de l'article
         * @var string
         */
        private $articleTitre;
        
        
        /**
         * Id du commentaire auquel repond$this, si c'est une réponse.
         * @var int
         */
        private $reponse;
        
        /**
         *  GETTERS
         */

        public function getAuteur():string
        {
            return $this->auteur;
        }
        
        public function getAvatar():string
        {
            return $this->avatar;
        }
    
        public function getArticle():int
        {
            return $this->article;
        }
        
        public function getArticleTitre():string
        {
            return $this->articleTitre;
        }
    
        public function getReponse()
        {
            return $this->reponse;
        }
    
        /***
         * 
         * SETTERS
         */
        
        /**
         * @param string/null $auteur
         */
        public function setAuteur($auteur)
        {
            if (!is_null($auteur)) {
                $this->auteur = $auteur;
            } else {
                $this->auteur = "Anonyme";
            }
        }
        
        /**
         * @param string/null $avatar
         */
        public function setAvatar($avatar)
        {
            if(!is_null($avatar)) {
                $this->avatar = "ressources/avatars/".$avatar;
            } else {
                $this->avatar = "ressources/avatars/inconnu.png";
            }
        }
    
        public function setArticle(int $article)
        {
            $this->article = $article;
        }
        
        public function setArticleTitre(string $articleTitre)
        {
            $this->articleTitre = $articleTitre;
        }
    
        /**
         * @param int/null $reponse
         */
        public function setReponse($reponse)
        {
            if(!is_null($reponse)) {
                $this->reponse = $reponse;
            }
        } 
    }
