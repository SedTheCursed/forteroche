<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title><?= $page->getTitre(); ?> | Admin </title>
		<!-- Bootstrap -->
	    <link
	    	rel="stylesheet"
	    	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	    	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	    	crossorigin="anonymous">
    	<link href="https://fonts.googleapis.com/css?family=Josefin+Slab|Lobster" rel="stylesheet"> 
    	<link rel="stylesheet" href="styles/style.css">
    	<link rel="stylesheet" href="styles/back.css">
    	<link rel="stylesheet" href="styles/impression.css" media="print">
    	<link rel="icon" type="image/x-icon" href="img/favicon.ico" />
    	<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /><![endif]-->

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>
		<div class="container">
			<!-- header -->
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-target">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="admin.php">Administration</a>
					</div>
					<div class="collapse navbar-collapse" id="navbar-collapse-target">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="admin.php">Accueil</a></li>
							<?php
							$navbarItems = new app\views\elements\NavbarItems();
							echo $navbarItems->view();
							?>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<?php
								    if((isset($_SESSION["user"]))
								        && $_SESSION["user"]->getAdmin()
								        && (get_class($_SESSION["user"]) !== "__PHP_Incomplete_Class")):
							    ?>
                                    <img alt="avatar de <?= $_SESSION["user"]->getPseudo(); ?>" src="<?php
                                        echo $_SESSION["user"]->getAvatar() ?>"/>
                                    <?= $_SESSION["user"]->getPseudo(); ?><b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                	<li><a href="index.php">Retour au site</a></li>
                                	<li><a href="?page=logout">Se déconnecter</a></li>
                                </ul>
                                <?php else: ?>								
									<img alt="avatar par defaut" src="ressources/avatars/inconnu.png"/>
									S'inscrire/Se connecter
								</a>
								<ul class="dropdown-menu">
                                	<li><a href="?page=login">Se connecter</a></li>
                                </ul>
								<?php endif ?>
						</ul>
					</div>
				</div>
			</nav>

			<!-- Le contenu -->
			<main>
				<?= $page->getContenu(); ?>
			</main>

			<!-- le footer -->
			<footer class="footer">
                <p>Billet simple pour l'Alaska - &copy;2017 Tous droits reservés à Jean Forteroche - <a href="?page=mentions">Mentions légales</a></p> 
			</footer>
		</div>

		<!--Bootstrap/JQuery -->
		<script
			src="http://code.jquery.com/jquery-3.2.1.min.js"
			integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
			crossorigin="anonymous">	  
		</script>
		<script 
			src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
			integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
			crossorigin="anonymous">
		</script>
		<?= $page->getScript(); ?>
	</body>
</html>
