<?php
namespace core\exceptions;

class LoginException extends \Exception
{
    protected $message = "Login ou mot de passe erronné.";
}
