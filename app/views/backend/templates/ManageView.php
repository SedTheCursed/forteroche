<?php
    namespace app\views\backend\templates;
    
    use app\views\elements\Commandes;
                
    abstract class ManageView extends BackView
    {
        use Commandes;
        
        /**
         * Entities à gerer
         * @var array
         */
        protected $items;
        /**
         * Page actuelle
         * @var int
         */
        protected $page;
        /**
         * Numero de la dernière page
         * @var int
         */
        protected $last;
        
        public function __construct(array $items, int $page, int $last)
        {
            $this->setItems($items);
            $this->setPage($page);
            $this->setLast($last);
            parent::__construct();
        }
        
        /**
         * Création des commandes de naviagtion entre les pages d'items
         */
        protected function commandes(string $items):string
        {
            $first = $this->page === 0;
            $last = $this->page === $this->last;
            $racine = "?page=manage".$items."&amp;table=";
            $url = [
                "first"     => $racine."0",
                "previous"  => $racine.($this->page-1),
                "next"      => $racine.($this->page+1),
                "last"      => $racine.$this->last
            ];
            
            return $this->layout($first, $last, $url);
        }
        
        /**
         * Création du tableau central de la page de gestion d'item
         * @param string $titre: titre de la page
         * @param array $head : colonnes de l'entête du tableau
         * @param array $body : les methodes à utiliser pour remplir les cellules de chaque ligne
         * @param string $entity : type d'item à gerer
         * @param bool $decorated : Doit-on utiliser la mise en page spécial, prevue pour les commentaires ?
         * @return string
         */
        protected function affichage(string $titre, array $head, array $body, string $entity, bool $decorated = false):string
        {
            ob_start();
            ?>
            <h1><?= $titre ?></h1>
            <table>
            	<thead>
                	<tr>
                        <?php foreach($head as $column): ?>
                        <th><?= $column;?></th>
                        <?php endforeach; ?>
                        <th colspan="2"></th>
                    </tr>
            	</thead>
                <tbody>
                	<?php foreach($this->items as $item):?>
                	<tr <?= ($decorated)?"class='decorated'":"";?>>
                		<?php foreach ($body as $column) :?>
                        <td><?= $item->$column(); ?></td>
                        <?php endforeach; ?>
                        <td <?= ($decorated)?"class='decorated'":"";?>>
                        	<a href="?page=modification<?= $entity; ?>&amp;id=<?= $item->getId(); ?>">
                        		<?= ($decorated)?"Mod.":"Modifier"; ?>
                    		</a>
                		</td>
                        <td <?= ($decorated)?"class='decorated'":"";?>>
                        	<a href="?page=delete<?= $entity; ?>&amp;id=<?= $item->getId(); ?>">
                        		<?= ($decorated)?"Suppr.":"Supprimer"; ?>
                    		</a>
                		</td>
                    </tr>
                	<?php endforeach; ?>
                </tbody>
			</table>
			<?= $this->commandes($entity."s"); ?>
            <?php
            return ob_get_clean();
        }
        
        protected function setScript()
        {
            $this->script="<script src='scripts/deleteItem.js'></script>";
        }
        
        protected function setItems(array $items)
        {
            $this->items = $items;
        }
        
        protected function setPage(int $page)
        {
            $this->page = $page;
        }
        
        protected function setLast(int $last)
        {
            $this->last = $last;
        }
    }
