<?php
    namespace app\views\backend\templates;
    
    use app\views\elements\View;
    
    abstract class BackView extends View
    {
        public function view(string $template="/templates/backend.php")
        {
            parent::view($template);
        }
    }
