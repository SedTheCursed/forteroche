<?php
namespace core\view;

trait Form
{
    private function champ(string $champ, string $message, bool $focus, string $type = "text", string $value=null):string
    {
        ob_start();
        ?>
            <div class="form-group">
            	<?php if (!empty($message)) :?>
            	<label for="<?= $champ; ?>"><?= $message; ?>&nbsp;:</label>
            	<?php endif;?>
            	<input type="<?= $type; ?>" class="form-control" name="<?= $champ; ?>" id="<?= $champ; ?>" required <?php 
            	   echo ($focus)? "autofocus ":"";
            	   echo ($value !== null)? 'value="'.$value.'"':"";
        	   ?>/>
            </div>
        <?php
        return ob_get_clean();
    }
}

