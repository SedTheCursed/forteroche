<?php
    namespace app\controllers;
    
    use app\views\backend\Page404;
    
    class Back extends Router
    {
        
        /**
         *  SETTERS
         */
        
        /**
         * Affecte la valeur true si un adminsitrateur est connecté.
         * @see \app\controllers\Router::setConnecte()
         */
        protected function setConnecte()
        {
            parent::setConnecte();
            $this->connecte = $this->connecte && $_SESSION["user"]->getAdmin();
        }
        
        
        /** 
         * @see \app\controllers\Router::setPage()
         */
        protected function setPage()
        {
            if (isset($_GET['page'])) {
                $url = $_GET['page'];
            } elseif (!empty($_GET) && !isset($_GET["success"])) {
                $url = "erreur";
            } else {
                $url = "accueil";
            }
            
            if ($url === "login" | $url === "connexion") {
                ($this->connecte)?header("location:admin.php"):"";
            } else {
                (!$this->connecte)?header("location:?page=login"):"";
            }
            
            switch($url) {
                case "accueil":
                    $comments = new \app\models\CommentairesManager($this->db);
                    $last = $comments->last();
                    $this->page = new \app\views\backend\HomeView($last);
                    break;
                    
                case "newBillets":
                    $this->page = new \app\views\backend\NewBilletView();
                    break;
                    
                case "manageBillets":
                    if (isset($_SESSION["retour"])) {
                        unset($_SESSION["retour"]);
                    }
                    
                    $manager = new \app\models\BilletsManager($this->db);
                    $table = (isset($_GET["table"]))?(int) $_GET["table"]:0;
                    $billets = $manager->paginated($table);
                    $last= $manager->totalPages();
                    $this->page = new \app\views\backend\ManageBilletsView($billets, $table, $last);
                    break;
                    
                case "modificationBillet" :
                    $manager = new \app\models\BilletsManager($this->db);
                    if (isset($_GET["id"])) {
                        $id = (int) $_GET["id"];
                        if (!isset($_SESSION["retour"])) {
                            $_SESSION["retour"] =  $_SERVER["HTTP_REFERER"];
                        }
                        
                        try {
                            $billet = $manager->one($id);
                            $this->page = new \app\views\backend\ModifierBilletView($billet, $_SESSION["retour"]);
                        } catch (\Exception $e) {
                            $this->page = new Page404();
                        }
                    } else {
                        $this->page = new Page404();
                    }
                    break;
                    
                case "newEdito":
                    $this->page = new \app\views\backend\NewEditoView();
                    break;
                    
                case "manageEdito":
                    if (isset($_SESSION["retour"])) {
                        unset($_SESSION["retour"]);
                    }
                    
                    $manager = new \app\models\EditosManager($this->db);
                    $table = (isset($_GET["table"]))?(int) $_GET["table"]:0;
                    $editos = $manager->paginated($table);
                    $last= $manager->totalPages();
                    $this->page = new \app\views\backend\ManageEditosView($editos, $table, $last);
                    break;
                    
                case "modificationEdito":
                    $manager = new \app\models\EditosManager($this->db);
                    if (isset($_GET["id"])) {
                        $id = (int) $_GET["id"];
                        if (!isset($_SESSION["retour"])) {
                            $_SESSION["retour"] =  $_SERVER["HTTP_REFERER"];
                        }
                        
                        try {
                            $edito = $manager->one($id);
                            $this->page = new \app\views\backend\ModifierEditoView($edito, $_SESSION["retour"]);
                        } catch (\Exception $e) {
                            $this->page = new Page404();
                        }
                    } else {
                        $this->page = new Page404();
                    }
                    break;
                    
                case "manageCommentaires":
                    if (isset($_SESSION["retour"])) {
                        unset($_SESSION["retour"]);
                    }
                    
                    $manager = new \app\models\CommentairesManager($this->db);
                    $table = (isset($_GET["table"]))?(int) $_GET["table"]:0;
                    $comments = $manager->paginated($table);
                    $last= $manager->totalPages();
                    $this->page = new \app\views\backend\ManageCommentairesView($comments, $table, $last);
                    break;
                    
                case "modificationCommentaire":
                    $manager = new \app\models\CommentairesManager($this->db);
                    if (isset($_GET["id"])) {
                        $id = (int) $_GET["id"];
                        if (!isset($_SESSION["retour"])) {
                            $_SESSION["retour"] =  $_SERVER["HTTP_REFERER"];
                        }
                        
                        try {
                            $comment = $manager->one($id);
                            $this->page = new \app\views\backend\ModifierCommentaireView($comment, $_SESSION["retour"]);
                        } catch (\Exception $e) {
                            $this->page = new Page404();
                        }
                    } else {
                        $this->page = new Page404();
                    }
                    break;
                    
                case "newUtilisateurs":
                    $this->page = new \app\views\backend\NewUserView();
                    break;
                    
                case "manageUtilisateurs":
                    if (isset($_SESSION["retour"])) {
                        unset($_SESSION["retour"]);
                    }
                    
                    $manager = new \app\models\UsersManager($this->db);
                    $table = (isset($_GET["table"]))?(int) $_GET["table"]:0;
                    $users = $manager->paginated($table);
                    $last= $manager->totalPages();
                    $this->page = new \app\views\backend\ManageUsersView($users, $table, $last);
                    break;
                    
                case "modificationUser":
                    $manager = new \app\models\UsersManager($this->db);
                    if (isset($_GET["id"])) {
                        $id = (int) $_GET["id"];
                        if (!isset($_SESSION["retour"])) {
                            $_SESSION["retour"] =  $_SERVER["HTTP_REFERER"];
                        }
                        
                        try {
                            $comment = $manager->one($id);
                            $this->page = new \app\views\backend\ModifierUserView($comment, $_SESSION["retour"]);
                        } catch (\Exception $e) {
                            $this->page = new Page404();
                        }
                    } else {
                        $this->page = new Page404();
                    }
                    break;
                    
                case "login":
                    $this->page = new \app\views\backend\LoginView();
                    break;
                    
                case "addBillet":
                    $manager = new \app\models\BilletsManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\BilletController($manager,$_POST,"add", $retour);
                    break;
                    
                case "updateBillet":
                    $manager = new \app\models\BilletsManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\BilletController($manager, $_POST, "update", $retour);
                    break;
                    
                case "deleteBillet":
                    $manager = new \app\models\BilletsManager($this->db);
                    $comments = new \app\models\CommentairesManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\BilletController($manager, $_GET, "delete", $retour, $comments);
                    break;
                    
                case "addEdito":
                    $manager = new \app\models\EditosManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\EditoController($manager, $_POST, "add", $retour);
                    break;
                    
                case "updateEdito":
                    $manager = new \app\models\EditosManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\EditoController($manager, $_POST, "update", $retour);
                    break;
                    
                case "deleteEdito":
                    $manager = new \app\models\EditosManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\EditoController($manager, $_GET, "delete", $retour);
                    break;
                    
                case "updateCommentaire":
                    $manager = new \app\models\CommentairesManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\CommentaireController($manager, $_POST, "update", $retour);
                    break;
                    
                case "deleteCommentaire":
                    $manager = new \app\models\CommentairesManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\CommentaireController($manager, $_GET, "delete", $retour);
                    break;
                    
                case "addUser":
                    $manager = new \app\models\UsersManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\UserController($manager, $_POST, "add", $retour);
                    break;
                    
                case "updateUser":
                    $manager = new \app\models\UsersManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $avatar = $_FILES["avatar"];
                    $this->page = new \app\controllers\UserController($manager, $_POST, "update", $retour, $avatar);
                    break;
                    
                case "deleteUser":
                    $manager = new \app\models\UsersManager($this->db);
                    $commentsManager = new \app\models\CommentairesManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\UserController($manager, $_GET, "delete", $retour, null, $commentsManager);
                    break;
                    
                case "connexion":
                    $manager = new \app\models\UsersManager($this->db);
                    $this->page = new \app\controllers\Connexion($manager, false);
                    break;
                    
                case "logout":
                    $this->page = new \app\controllers\Deconnexion(false);
                    break;
                    
                default :
                    $this->page = new Page404();
            }
        }
    }
