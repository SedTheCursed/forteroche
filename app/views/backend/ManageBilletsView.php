<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\ManageView;
                
    class ManageBilletsView extends ManageView
    {        
        protected function commandes(string $items = "Billets"):string
        {
            return parent::commandes($items);
        }
        
        protected function setTitre()
        {
            $this->titre = "Billets";
        }
    
        protected function setContenu()
        {
            $titre = "Billets";
            $head = ["Date","Titre"];
            $body = ["getDateAjout","getTitre"];
            $entity = "Billet";
            
            $this->contenu = $this->affichage($titre, $head, $body, $entity);
        }
    }
