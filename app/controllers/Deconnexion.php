<?php
    namespace app\controllers;
    
    class Deconnexion
    {
        /**
         * Adresse de retour après la déconnexion
         * @var string
         */
        private $retour;
        
        public function __construct(bool $frontend = true)
        {
            $this->setRetour($frontend);
        }
        
        /**
         * Execution de la déconnexion
         */
        public function exec()
        {
            //destruction des données
            $_SESSION=array();
            session_destroy();
            
            //reinitialisation des cookies
            setcookie("login","");
            setcookie("password","");
            
            header("location:".$this->retour);
        }
        
        /**
         *  SETTERS
         */
        
        private function setRetour(bool $frontend)
        {
            if ($frontend) {
                if (isset($_SESSION["retour"])) {
                    $this->retour = $_SESSION["retour"];
                } else {
                    $this->retour = "index.php";
                }
            } else {
                $this->retour = "admin.php";
            }
        }
    }
