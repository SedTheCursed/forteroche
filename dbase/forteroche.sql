-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1:3306
-- Généré le :  Jeu 14 Septembre 2017 à 22:35
-- Version du serveur :  5.6.34-log
-- Version de PHP :  7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `forteroche`
--

-- --------------------------------------------------------

--
-- Structure de la table `billets`
--

CREATE TABLE IF NOT EXISTS `billets` (
  `bill_id` int(11) NOT NULL,
  `bill_titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bill_contenu` text COLLATE utf8_unicode_ci NOT NULL,
  `bill_date_ajout` datetime NOT NULL,
  `bill_date_modif` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `billets`
--

INSERT INTO `billets` (`bill_id`, `bill_titre`, `bill_contenu`, `bill_date_ajout`, `bill_date_modif`) VALUES
(1, 'Silent House, Cold ashes (part I)', '<p>Le silence et le bruit.<br/>Le bruit et le silence.<br/>Le bruit est souvent fatigant, mais il a un côté rassurant. Une pièce emplie de voix est souvent le plus court chemin vers une migraine carabinée, mais on s''y sens que entouré, même si de meilleurs que moi, on fait justement remarqué que l''on est jamais plus seul qu''au milieu d''une foule. Le silence, au contraire, est apaisant, mais il peut facilement se révéler inquiétant. Un lieu vide est calme et permet de se focaliser sur ses pensées, et c''est aussi sa plus grande faiblesses car il ne permet pas d’échapper à icelles.<br/>Dans tous les cas, lorsqu''on entre dans un lieu habitué à l''un, si c''est l''autre qui y règne, ce n''est jamais bon signe. J''ai souvenir d''un matin, où, enfant, j''ai entendu des voix dans la salle de séjour proche de ma chambre. Pas vraiment, le meilleur souvenir de mon existence, que cette réunion de famille au petit matin, c''est certain.</p>', '2017-08-09 17:49:24', '2017-08-09 18:48:02'),
(2, 'Silent House, Cold ashes (part II)', '<p>Et ce fut le silence qui m''indiqua que quelque chose n''allait pas ce soir-là. Non pas que notre maison fut, à l''acoutumée, particulièrement bruyante ou animée. Nous ne vivions que tous les deux et étions loin d''être des animaux sociaux.<br/>Je revenais d''un week-end de séminaire sur l''influence du cercle et de l''anneau sur les diverses sociétés, sujet intéressant, qui n''a que peu de rapport, malgré ce que l''on pourrait le penser, avec le soleil et la lune, mais surtout qui n''a aucune influence sur notre histoire. En poussant la porte, j''ai sû qu''elle n''était pas là. Pas de télé ou de musique, ni le bruit d''une quelconque activité. Elle aurait pu lire, dormir, ou occupé à quelque chose ne faisant pas de bruit. Mais nous vivions ensemble depuis suffisemment longtemps pour avoir developpé cette osmose permettant de savoir si l''autre est là ou non. En soit, cela n''avait rien d''inquietant, elle aurait pu s''absenter, d''autant que l''heure de mon retour lui était inconnue.<br/>Le silence l''était, lui. Et par silence, je ne parle pas du calme qui règnait, mais de l''absence des bruits blancs que dont nous prenons l''habitude au point de ne plus y faire attention, ou même de les entendre. Le tic-tac de la pendule, le ronflement du moteur du refrigerateur, le sifflement du ventilateur d''un ordinateur ou le crépitement de la cheminée.</p>', '2017-08-10 16:01:50', NULL),
(3, 'Silent House, Cold Ashes (Part I-II)', '<p>Le silence et le bruit.<br/>Le bruit et le silence.<br/>Le bruit est souvent fatigant, mais il a un côté rassurant. Une pièce emplie de voix est souvent le plus court chemin vers une migraine carabinée, mais on s''y sens que entouré, même si de meilleurs que moi, on fait justement remarqué que l''on est jamais plus seul qu''au milieu d''une foule. Le silence, au contraire, est apaisant, mais il peut facilement se révéler inquiétant. Un lieu vide est calme et permet de se focaliser sur ses pensées, et c''est aussi sa plus grande faiblesses car il ne permet pas d’échapper à icelles.<br/>Dans tous les cas, lorsqu''on entre dans un lieu habitué à l''un, si c''est l''autre qui y règne, ce n''est jamais bon signe. J''ai souvenir d''un matin, où, enfant, j''ai entendu des voix dans la salle de séjour proche de ma chambre. Pas vraiment, le meilleur souvenir de mon existence, que cette réunion de famille au petit matin, c''est certain.<br/>Et ce fut le silence qui m''indiqua que quelque chose n''allait pas ce soir-là. Non pas que notre maison fut, à l''accoutumée, particulièrement bruyante ou animée. Nous ne vivions que tous les deux et étions loin d''être des animaux sociaux.<br/>Je revenais d''un week-end de séminaire sur l''influence du cercle et de l''anneau sur les diverses sociétés, sujet intéressant, qui n''a que peu de rapport, malgré ce que l''on pourrait le penser, avec le soleil et la lune, mais surtout qui n''a aucune influence sur notre histoire. En poussant la porte, j''ai su qu''elle n''était pas là. Pas de télé ou de musique, ni le bruit d''une quelconque activité. Elle aurait pu lire, dormir, ou occupé à quelque chose ne faisant pas de bruit. Mais nous vivions ensemble depuis suffisamment longtemps pour avoir développé cette osmose permettant de savoir si l''autre est là ou non. En soit, cela n''avait rien d''inquiétant, elle aurait pu s''absenter, d''autant que l''heure de mon retour lui était inconnue.<br/>Le silence l''était, lui. Et par silence, je ne parle pas du calme qui régnait, mais de l''absence des bruits blancs que dont nous prenons l''habitude au point de ne plus y faire attention, ou même de les entendre. Le tic-tac de la pendule, le ronflement du moteur du réfrigérateur, le sifflement du ventilateur d''un ordinateur ou le crépitement de la cheminée.</p>', '2017-08-18 11:50:03', NULL),
(4, 'The tar pit', '<p>Imaginez que vous viviez enfonc&eacute; jusqu''&agrave; la ceinture dans une fosse de goudron, dont il vous faut vous extirper d&egrave;s que vous voulez faire quelque chose, d''un tant soit peu &eacute;volu&eacute;. Les actes quasi-automatiques, comme manger, prendre une douche ou &eacute;vacuer ses nutriments, ne sont pas emp&ecirc;ch&eacute;s. Par contre, toute forme de travail l''est, qu''il soit physique ou intellectuel.<br />Pour vous lib&eacute;rer, nous n''avez souvent que deux options : en sortir seul ou s''appuyer sur l''aide d''un tiers. La premi&egrave;re est ext&eacute;nuante, au point qu''il ne vous reste souvent plus d''&eacute;nergie pour la tache &agrave; accomplir. La seconde est lassante plus ou moins court terme. Les aidants ne peuvent pas toujours &ecirc;tre pr&eacute;sents et, ce qui est compr&eacute;hensible, peuvent en avoir marre d''aider une personne qui n''avance pas et retombe sans cesse dans cette fosse. Quant &agrave; l''aid&eacute;, il se sent frustr&eacute; d''avoir besoin de cette aide permanente et ne pas pouvoir s''extraire facilement de lui-m&ecirc;me, et quelque peu honteux de devoir impos&eacute; aux tiers cette relation de d&eacute;pendance.<br />Comme dans toutes d&eacute;pendances, il se d&eacute;veloppe ce facteur amour/haine avec l''objet. Amour filiale, amour fraternel, amour au sens large, mais est-ce vraiment de l''amour ou seulement de la reconnaissance ? La Haine prends aussi tant de formes, jalousie, peur de l''abandon, sentiment de trahison. Bref les bonne bases d''une relation toxique, surtout si l''aidant ne l''est pas tant que cela, s''il vous aide d''une main pour mieux vous enfoncer de l''autre, pour mieux flatter son propre &eacute;go.<br />Tout cela n''est qu''une vaine tentative de justification des raisons pour lesquelles je n''arrive pas pas &agrave; &eacute;crire, danser ou programmer, alors que ce sont trois choses que j''aime &eacute;norm&eacute;ment. Cela et la crainte de ne pas &ecirc;tre &agrave; la hauteur de la tache. Mais si on ne pratique pas, on ne risque pas d''&ecirc;tre &agrave; la hauteur, vous &ecirc;tes, tout comme je le suis, parfaitement au courant de cela. Et pourtant je reste coinc&eacute; et continue &agrave; me noyer dans ma fosse.</p>', '2017-08-23 16:11:57', '2017-09-13 15:50:03');

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE IF NOT EXISTS `commentaires` (
  `com_id` int(11) NOT NULL,
  `com_contenu` text COLLATE utf8_unicode_ci NOT NULL,
  `com_auteur` int(11) DEFAULT NULL,
  `com_date_ajout` datetime NOT NULL,
  `com_date_modif` datetime DEFAULT NULL,
  `com_article` int(11) NOT NULL,
  `com_reponse` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `commentaires`
--

INSERT INTO `commentaires` (`com_id`, `com_contenu`, `com_auteur`, `com_date_ajout`, `com_date_modif`, `com_article`, `com_reponse`) VALUES
(1, '<p>Content de pouvoir vous relire.<br/>Six ans depuis votre dernier livre, cela faisait longtemps.</p>', 3, '2017-08-16 14:35:06', NULL, 1, NULL),
(2, '<p>Content de réécrire, de mon côté.<br/> J''espère que vous aller aimer ce roman.</p>', 1, '2017-08-16 14:36:15', NULL, 1, 1),
(3, '<p>Le début laisse présager un ton plus sombre que vos livres précédents.</p>', NULL, '2017-08-16 14:40:13', NULL, 1, NULL),
(4, '<p>De même, je déteste vraiment rentrer dans une maison vide.</p>', NULL, '2017-08-16 14:46:07', NULL, 2, NULL),
(5, '<p>Hate de voir cela alors.</p>', 3, '2017-08-17 12:02:18', NULL, 1, 1),
(6, '<p>Sentiment connu de <strong>tous les introvertis.<br /></strong>Donc je comprends le sentiment.</p>\r\n<p style="text-align: center;">Nous sommes donc de tous c&oelig;ur avec vous !</p>', NULL, '2017-08-23 18:37:18', NULL, 4, NULL),
(14, '<p>Heureusement que les forces reviennent lorsque on s''adonne aux activités que le goudron bloquait.</p><p></p>', 3, '2017-08-24 10:37:35', NULL, 4, NULL),
(15, '<p>Oui, en effet. <img src="https://cloud.tinymce.com/stable/plugins/emoticons/img/smiley-wink.gif" alt="wink" /></p>', 1, '2017-08-24 10:38:47', NULL, 4, 14),
(19, '<p>C''est le cas de tous le monde, je pense.</p>', NULL, '2017-08-24 15:27:49', NULL, 2, 4),
(21, '<p><strong>@Anonyme </strong>Je confirme.</p>', NULL, '2017-08-24 16:28:30', NULL, 2, 4),
(24, '<p>&lt;Message Effacé&gt;</p><p></p>', 4, '2017-09-06 10:19:38', '2017-09-07 13:33:12', 3, NULL),
(25, '<p>Oui, en effet.</p>', 11, '2017-09-06 10:20:59', NULL, 3, 24),
(26, '<p><strong>@Jean Forteroche </strong>Cela reste encore &agrave; voir, je le crains, sans vouloir &ecirc;tre d&eacute;faitiste.</p>', 11, '2017-09-06 11:29:33', '2017-09-12 22:51:39', 4, 14),
(27, '<p><strong>@Tinual </strong>Cad &eacute; mar at<span style="font-size: large;">&aacute; t</span><span style="font-size: large;">&uacute; ?<br /></span></p>', NULL, '2017-09-06 15:40:31', NULL, 3, 24),
(28, '<p>C''&eacute;tait moi au-dessus.&nbsp;<img src="https://cloud.tinymce.com/stable/plugins/emoticons/img/smiley-tongue-out.gif" alt="tongue-out" /></p>\r\n<p>(oui, je suis un brin t&ecirc;te en l''air, mais nous le savons, non ?)</p>', 4, '2017-09-06 15:41:41', '2017-09-13 15:52:25', 3, 24),
(55, '<p>&lt;Message Effacé&gt;</p><p></p>', 2, '2017-09-07 15:11:13', '2017-09-07 15:12:01', 4, NULL),
(56, '<p>&lt;Message Effacé&gt;</p><p></p>', 2, '2017-09-07 15:11:49', '2017-09-07 15:12:01', 4, 55),
(58, '<p>&lt;Message Effacé&gt;</p><p></p>', NULL, '2017-09-13 20:20:19', '2017-09-13 20:23:16', 4, NULL),
(60, '<p>&lt;Message Effacé&gt;</p><p></p>', 2, '2017-09-13 20:22:08', '2017-09-13 21:32:34', 4, 58);

-- --------------------------------------------------------

--
-- Structure de la table `editos`
--

CREATE TABLE IF NOT EXISTS `editos` (
  `ed_id` int(11) NOT NULL,
  `ed_titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ed_contenu` text COLLATE utf8_unicode_ci NOT NULL,
  `ed_date_ajout` datetime NOT NULL,
  `ed_date_modif` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `editos`
--

INSERT INTO `editos` (`ed_id`, `ed_titre`, `ed_contenu`, `ed_date_ajout`, `ed_date_modif`) VALUES
(1, 'Bienvenue pour un voyage avec un billet simple pour l''Alaska', '<p>Bonjour, je suis Jean Forteroche, com&eacute;dien et auteur.<br />Je pr&eacute;pare actuellement mon prochain roman, <strong>Billet simple pour l''Alaska</strong>. Si vous connaissez mon oeuvre, vous devez savoir mon interet pour la Belle &Eacute;poque, c''est pour cela que, &agrave; l''instar des r&eacute;cits de l''&eacute;poque, j''ai d&eacute;cid&eacute; de l''&eacute;crire et de le pr&eacute;-publier de mani&egrave;re feuilletonnante. <em>Ergo</em> le present site.<br />Comme son nom l''indique, il se passe dans notre monde, et &agrave; notre &eacute;poque, mais cela ne nous emp&ecirc;chera pas de visiter quelques visages et lieux familiers.</p>\r\n<p>Sur ce, je vous souhaite une agr&eacute;able lecture et un bon voyage vers Anchorage.</p>', '2017-08-11 17:31:00', '2017-09-13 16:01:07');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `us_id` int(11) NOT NULL,
  `us_login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `us_pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `us_avatar` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `us_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `us_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `us_admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`us_id`, `us_login`, `us_pseudo`, `us_avatar`, `us_password`, `us_email`, `us_admin`) VALUES
(1, 'auteur', 'Jean Forteroche', '1.jpg', '$2y$10$ZHs5NQFju.X0yFRf6NtQe.dYL5YI0gCFWyMMvLRrPuGGVPPIyb8M2', 'sed.robert@gmail.com', 1),
(2, '', '&lt;UTILISATEUR SUPPRIMÉ&gt;', NULL, NULL, NULL, 1),
(3, 'toto', 'Jean Martin', '3.png', 'Coin-coin', 'mail@exemple.com', NULL),
(4, 'sed', 'Sed the Cursed', '.jpg', '$2y$10$dqWMOeoUGvmwvb4gFOdkr.z0GlyFZW/sBJwangIbFLTcHeGJRZDLW', 'sedthecursed@gmail.com', NULL),
(11, 'tinual', 'Tinual', '11.jpg', '$2y$10$VjrBm4eiPThAIO872j41ZO9FtsKVHyXQLQEO3Hxspl8KeImeyZrgW', 'tinual@hotmail.com', NULL),
(12, 'teo', 'Moman', '12.gif', '$2y$10$iuRK4ImtYvN7gvxUDs85NO8OrKwngxkinr/qJ4oYNfmkLZjjGQdjG', 'moman.attila@gmail.com', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `billets`
--
ALTER TABLE `billets`
  ADD PRIMARY KEY (`bill_id`);

--
-- Index pour la table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`com_id`);

--
-- Index pour la table `editos`
--
ALTER TABLE `editos`
  ADD PRIMARY KEY (`ed_id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`us_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `billets`
--
ALTER TABLE `billets`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT pour la table `editos`
--
ALTER TABLE `editos`
  MODIFY `ed_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `us_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
