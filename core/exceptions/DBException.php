<?php
namespace core\exceptions;

class DBException extends \Exception
{
    protected $message = "Impossible de se connecter à la base de données. Veuillez réessayer plus tard.";
}

