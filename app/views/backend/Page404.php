<?php
    namespace app\views\backend;
    
    use app\views\elements\Misdirection;
    use app\views\backend\templates\BackView;
    
    class Page404 extends BackView
    {
        
        use Misdirection {
            setContenu as protected hawaii;
        }
    
        protected function setContenu(string $url="admin.php")
        {
            $this->hawaii($url);
        }
    }
