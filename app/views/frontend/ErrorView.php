<?php
    namespace app\views\frontend;
    
    class ErrorView extends FrontView
    {
        /**
         * Erreur ayant empeche l'affichage correcte une page
         * @var \Exception
         */
        private $erreur;
        
        public function __construct(\Exception $erreur)
        {
            $this->setErreur($erreur);
            parent::__construct();
        }
        
        private function setErreur(\Exception $erreur)
        {
            $this->erreur = $erreur;
        }
        
        protected function setTitre()
        {
            $this->titre = "Accueil";
        }
        
        protected function setContenu()
        {
            ob_start();
            ?>
            <h1>Une erreur s'est produite</h1>
            <p>
            	<?= $this->erreur->getMessage() ?>
            </p>
            <p>Revenir à <a href="index.php">l'accueil.</a></p>
            <?php
            $this->contenu = ob_get_clean();
        }
    }
