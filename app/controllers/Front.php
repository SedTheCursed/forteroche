<?php
    namespace app\controllers;
    
    use app\views\frontend\Page404;
    
    class Front extends Router
    {
        /**
         * @see \app\controllers\Router::setPage()
         */
        protected function setPage()
        {
            if (isset($_GET['page'])) {
                $url = $_GET['page'];
            } elseif (!empty($_GET)) {
                $url = "erreur";
            } else {
                $url = "accueil";
            }
            
            
            $needConnexion = ["profil", "updateMessage", "delete", "updateUser", "deleteUser", "logout"];
            $needDeconnexion = ["inscription","login", "newUser", "connexion"];
            
            if (in_array($url, $needConnexion)) {
                (!$this->connecte)?header("location:?page=login"):"";
            } elseif (in_array($url, $needDeconnexion)) {
                ($this->connecte)?header("location:index.php"):"";
            }
            
            switch ($url) {
                case "accueil":
                    $billets = new \app\models\BilletsManager($this->db);
                    $editos = new \app\models\EditosManager($this->db);
                    $billet = $billets->last();
                    $edito = $editos->last();
                    $this->page = new \app\views\frontend\HomeView($billet, $edito);
                    break;
                    
                case "billet":
                    if (isset($_GET["id"])) {
                        $billets = new \app\models\BilletsManager($this->db);
                        $commentsDB = new \app\models\CommentairesManager($this->db);
                        $id = (int) $_GET["id"];
                        
                        try {
                            $billet = $billets->one($id);
                            $commentaires = $commentsDB->articleComments($id);
                            $this->page = new \app\views\frontend\BilletView($billet, $commentaires);
                        } catch (\Exception $e) {
                            $this->page = new Page404();
                        }
                    } else {
                        $this->page = new Page404();
                    }
                    break;
                    
                case "archives":
                    $billets=new \app\models\BilletsManager($this->db);
                    $archives=$billets->all();
                    $this->page = new \app\views\frontend\ArchivesView($archives);
                    break;
                    
                case "auteur":
                    $this->page = new \app\views\frontend\AuteurView();
                    break;
                    
                case "mentions":
                    $this->page = new \app\views\frontend\MentionsView();
                    break;
                    
                case "inscription":
                    $_SESSION["retour"] = $_SERVER["HTTP_REFERER"];
                    $this->page = new \app\views\frontend\InscriptionView();
                    break;
                    
                case "login":
                    $_SESSION["retour"] = $_SERVER["HTTP_REFERER"];
                    $this->page = new \app\views\frontend\LoginView();
                    break;
                    
                case "profil":
                    if (isset($_SERVER["HTTP_REFERER"]) && !preg_match("#page=inscription#", $_SERVER["HTTP_REFERER"])) {
                        $_SESSION["retour"] = $_SERVER["HTTP_REFERER"];
                    }
                    $this->page = new \app\views\frontend\ProfilView($_SESSION["user"]);
                    break;
                    
                case "newMessage":
                    $manager = new \app\models\CommentairesManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\CommentaireController($manager, $_POST, "add", $retour);
                    break;
                    
                case "updateMessage":
                    $manager = new \app\models\CommentairesManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\CommentaireController($manager, $_POST, "update", $retour);
                    break;
                    
                case "delete":
                    $manager = new \app\models\CommentairesManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $this->page = new \app\controllers\CommentaireController($manager, $_GET, "delete", $retour);
                    break;
                    
                case "newUser":
                    $manager = new \app\models\UsersManager($this->db);
                    $retour = "?page=inscription";
                    $this->page = new \app\controllers\UserController($manager, $_POST, "add", $retour);
                    break;
                    
                case "updateUser":
                    $manager = new \app\models\UsersManager($this->db);
                    $retour = preg_replace("#&((success)|(erreur))=.*$#", "", $_SERVER["HTTP_REFERER"]);
                    $avatar = (isset($_FILES["avatar"]))?$_FILES["avatar"]:null;
                    $_POST["id"] = $_SESSION["user"]->getId();
                    $_POST["side"] = "front";
                    $this->page = new \app\controllers\UserController($manager, $_POST, "update", $retour, $avatar);
                    break;
                    
                case "deleteUser":
                    $manager = new \app\models\UsersManager($this->db);
                    $commentsManager = new \app\models\CommentairesManager($this->db);
                    $retour = "?page=logout";
                    if (isset($_POST["deleteAccount"]) && isset($_POST["id"]) && (int) $_POST["id"]===$_SESSION["user"]->getId()) {
                        $this->page = new \app\controllers\UserController($manager, $_POST, "delete", $retour, null, $commentsManager);
                    }
                    break;
                    
                case "connexion":
                    $manager= new \app\models\UsersManager($this->db);
                    $this->page = new \app\controllers\Connexion($manager);
                    break;
                    
                case "logout":
                    if (isset($_SERVER["HTTP_REFERER"]) && !preg_match("#page=profil#", $_SERVER["HTTP_REFERER"])) {
                        $_SESSION["retour"] = $_SERVER["HTTP_REFERER"];
                    }
                    $this->page = new \app\controllers\Deconnexion();
                    break;
                    
                default:
                    $this->page = new Page404();
            }
        }
    }
