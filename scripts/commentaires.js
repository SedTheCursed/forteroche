$(function(){	
	$(".repondre").each(function(index){
		$(this).on("click",function(e){
			e.preventDefault();
			var current = $(e.target.parentNode.parentNode.lastElementChild),
				currentId = current.attr("id");
			
			$(".reponseForm:not(#"+currentId+")").hide();
			current.toggle();

			if($(e.target.parentNode.parentNode.parentNode).hasClass("reponse")) {
				var auteur = $(e.target.parentNode.parentNode.firstElementChild.firstElementChild).text(),
					editorId = $(e.target.parentNode.nextElementSibling.firstElementChild.lastElementChild).attr("id");
				
				tinyMCE.get(editorId).focus();
				tinyMCE.activeEditor.setContent("<strong>@"+auteur+"</strong>");
				tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.getBody(), true);
				tinyMCE.activeEditor.selection.collapse(false);
			}
		});
	});
	
	$(".reponseForm input[type=reset]").on("click",function(e){
		$(e.target).parent().parent().hide();
	});
	
	$(".modifier").each(function(index){
		$(this).on("click",function(e){
			e.preventDefault();
			
			var currentForm = $(e.target).parent().prev(),
				currentComment = currentForm.prev(),
				currentId = currentForm.attr("id"),
				content = currentComment.html(),
				editorId = currentForm.children().eq(1).attr("id");
			
			console.log(content);
			$(".modifForm:not(#"+currentId+")").hide();
			$(".modifForm:not(#"+currentId+")").prev().show();
			currentForm.toggle();
			currentComment.toggle();
			
			tinyMCE.get(editorId).focus();
			tinyMCE.activeEditor.setContent(content);
			tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.getBody(), true);
			tinyMCE.activeEditor.selection.collapse(false);
		})
	});
	
	$(".modifForm input[type=reset]").on("click",function(e){
		var form = $(e.target).parent().parent()
		form.hide();
		form.prev().show();		
	});
	
	$(".supprimer").each(function(){
		$(this).on("click", function(e){
			var confirmation=confirm("Voulez-vous vraiment effacer ce commentaire ?");
			
			if(!confirmation) {
				e.preventDefault();
			}
		});
	});
});
