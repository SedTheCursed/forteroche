<?php
    namespace app\views\frontend;
    
    use app\entities\Billet;
    use app\entities\Commentaire;
    use app\views\elements\Commandes;
                                                
    class BilletView extends FrontView
    {
        use Commandes;
        
        /**
         * Billet affiché par la page
         * @var Billet
         */
        private $billet;
        
        /**
         * Nombre de commentaires répondant au billet  
         * @var int
         */
        private $nbCommentaires;
        
        /**
         * Commentaires repondant au billet
         * @var array
         */
        private $commentaires;
        
        public function __construct(Billet $billet, array $commentaires)
        { 
            $this->setBillet($billet);
            $this->setNbCommentaires($commentaires);
            $this->setCommentaires($commentaires);
            parent::__construct();
        }
        
        
        /**
         * Paramêtre les adresse des boutons de navigation
         */
        private function commandes():string
        {
            $first = $this->billet->isFirst();
            $last = $this->billet->isLast();
            $racine = "?page=billet&amp;id=";
            $url = [
                "first"     =>  $racine.$this->billet->getFirstId(),
                "previous"  =>  $racine.($this->billet->getId()-1),
                "next"      =>  $racine.($this->billet->getId()+1),
                "last"      =>  $racine.$this->billet->getLastId()
            ];
            
            return $this->layout($first, $last, $url);
        }
        
        /**
         * Paramêtre l'affichage des commentaires
         */
        private function commentView(Commentaire $commentaire):string
        {
            //Determine sur l'utilisateur connecté est l'auteur du commentaire.
            $auteur=isset($_SESSION["user"]) && $_SESSION["user"]->getPseudo() === $commentaire->getAuteur();
            
            ob_start();
            ?>
            <div><img alt="Avatar de <?= $commentaire->getAuteur()?>" src="<?= $commentaire->getAvatar(); ?>" class="avatar"/></div>
            <div>
                <p><strong class="auteur"><?= $commentaire->getAuteur(); ?></strong> - <?php
                echo $commentaire->getDateAjout();
                if ($commentaire->getDateModif() !== null) {
                    echo " ".$commentaire->getDateModif();
                } ?></p>
                <?= $commentaire->getContenu(); ?>
                <?php
                //Rien n'est placé sur un commentaire effacé
                if (!preg_match("#<p>&lt;Message Effacé&gt;</p><p></p>#", $commentaire->getContenu())) {
                    if($auteur) { //Ajout d'un champ de modification si le visiteur est l'auteur
                        echo $this->commentField("modifier", $commentaire);
                    }
                ?>
                <p>
                	<a href="" class="repondre">Répondre</a>
                	<?php if ($auteur) : ?>
					<a href="" class="modifier">Modifier</a>
					<a href="?page=delete&amp;id=<?= $commentaire->getId(); ?>" class="supprimer">Supprimer</a>
                	<?php endif; ?>
            	</p>
            	<?php 
            	   echo $this->commentField("reponse",$commentaire);
                }
                ?>
            </div>
            <?php
            return ob_get_clean();
        }
        
        /**
         * Création des divers formulaires de commentaires, selon que cela soit un nouveau commentaire,
         * une reponse ou une modification
         */
        private function commentField(string $type, Commentaire $commentaire=null):string
        {
            if ($type==="modifier") {
                $action = "?page=updateMessage";
                $confirm = "Modifier";
                $id = '"modifier'.$commentaire->getId().'" class="modifForm"';
            } else {
                $action = "?page=newMessage";
                $confirm = "Ajouter";
                
                if($type==="comment") {
                    $id = '"commentForm"';
                } else {
                    $id = '"reponse'.$commentaire->getId().'" class="reponseForm"';
                }
            }
            
            ob_start();
            ?>
            <form action="<?= $action; ?>" method="post" id=<?= $id;?>>
            <?php if ($type==="reponse") :?>
            	<div class="reponseTextarea">
        	<?php endif;?>
            	<textarea name="contenu"></textarea>
        	<?php if ($type==="reponse") :?>
        		</div>
    		<?php endif;?>
            	<p>
        			<?php if (isset($_SESSION["user"])):?>
        			<input type="hidden" name="auteur" value="<?= $_SESSION["user"]->getId(); ?>" />
        			<?php endif; ?>
        			<input type="hidden" name="article" value="<?= $this->billet->getId();?>"/>
        			<?php if ($type==="reponse"):?>
        			<input type="hidden" name="reponse" value="<?= 
        			($commentaire->getReponse() !== null)?$commentaire->getReponse():$commentaire->getId(); ?>" />
        			<?php elseif ($type==="modifier"):?>
        			<input type="hidden" name="id" value="<?= $commentaire->getId(); ?>" />
        			<?php endif; ?>
                    <input type="submit" value="<?= $confirm; ?>"/>
                    <input type="reset" value="Annuler" />
    			</p>
        	</form>
        	<?php
        	return ob_get_clean();
        }

        /**
         *  SETTERS
         */
        
        private function setBillet(Billet $billet)
        {
            $this->billet = $billet;   
        }
        
        private function setNbCommentaires(array $commentaires)
        {
            $this->nbCommentaires = count($commentaires);
        }
    
        /**
         * Organise les commentaires afin que les réponses suivent le commentaire auxquel elles repondent
         */
        private function setCommentaires(array $commentaires)
        {
            $ordreCommentaire = [];
            foreach ($commentaires as $commentaire) {
                if (is_null($commentaire->getReponse())) {
                    $ordreCommentaire[$commentaire->getId()][0] = $commentaire;
                } else {
                    $ordreCommentaire[$commentaire->getReponse()][] = $commentaire;
                }
            }
            
            $this->commentaires = $ordreCommentaire;
        }
        
        protected function setTitre()
        {
            $this->titre = $this->billet->getTitre();
        }
        
        protected function setContenu()
        {
            ob_start();
            echo $this->commandes();
            ?>
            <article>
            	<h1><?= $this->billet->getTitre(); ?></h1>
            	<?= $this->billet->getContenu(); ?>
            	<p class="info">
            		<span>
    					<?= (count($this->commentaires)>0)?"Commentaires(".$this->nbCommentaires.")":"Pas de commentaires";?>
            		</span>
            		<span class="date-article">
        	<?php
            echo $this->billet->getDateAjout();
            if ($this->billet->getDateModif() !== null) {
               echo " ".$this->billet->getDateModif();
            }
            ?>
            		</span>
            	</p>
            </article>
            <?= $this->commandes(); ?>
            <div id="commentaires">
            	<h2>Commentaires</h2>
        	<?php
        	   if (isset($_GET["erreur"])) {
        	       echo "<p class='alert'>";
        	       switch($_GET["erreur"]) {
        	           case "contenu":
        	               echo "Le commentaire est vide.";
        	               break;
        	               
    	               default:
        	              echo "Il y a eu un problème avec l'enregistrement du commentaire.";     
        	       }
        	       echo "</p>";
        	   }
        	   foreach ($this->commentaires as $commentaire): ?>
            	<div class="commentaire">
            		<div>
        	<?php    
                echo $this->commentView($commentaire[0]);
            ?>
            		</div>
            		<div class="reponses">           		
            <?php
                $nbReponse = count($commentaire);
                for ($i=1; $i<$nbReponse; $i++) : ?>
                    	<div class="reponse">
                        	<?= $this->commentView($commentaire[$i]);?>
                        </div>
        	<?php endfor;?>
            		</div>
                </div>
            <?php
                endforeach;
                echo $this->commentField("comment");
            ?>          	
            </div>
            <?php
            $this->contenu = ob_get_clean();
        }
        
        protected function setScript()
        {
            $this->script = "<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=lll7oi734hwr9btz7jcf2to0bavorp8gt2okmvt04rrgro7h'></script>
            <script src='scripts/tinyMCE.js'></script>
            <script src='scripts/commentaires.js'></script>";
        }
    }
