<?php
namespace app\models;

use core\models\Manager;
use app\entities\User;
use core\exceptions\loginException;

class UsersManager extends Manager
{
    public function addNew(string $login, string $email, string $password, string $pseudo = null, bool $admin = false)
    {
        $pseudo = ($pseudo !== null)? $pseudo : ucfirst($login);
        $request = "INSERT INTO utilisateurs(us_login, us_pseudo, us_email, us_password, us_admin)
            VALUES(:login, :pseudo, :email, :password, :admin)";
        $donnees = [
            "login"     =>  $login,
            "pseudo"    =>  $pseudo,
            "email"     =>  $email,
            "password"  =>  $password,
            "admin"     =>  $admin
        ];
        
        $this->manage($request, $donnees);
    }
    
    public function one(int $id): User
    {
        $request = "SELECT 
                us_id AS id,
                us_login AS login,
                us_pseudo AS pseudo,
                us_avatar AS avatar,
                us_email AS email,
                us_admin AS admin
            FROM utilisateurs
            WHERE us_id=?";
        $settings = [$id];
        
        $donnees = $this->query($request,$settings)[0];
        
        return new User($donnees);
    }
    
    /**
     * Recuperation par 20 pour paginer la liste des utilisateurs dans l'interface de gestion
     */
    public function paginated(int $page):array
    {
        $users = [];
        $request ="SELECT
                            us_id AS id,
                            us_avatar AS avatar,
                            us_pseudo AS pseudo,
                            us_admin AS admin
                        FROM utilisateurs
                        ORDER BY us_id DESC
                        LIMIT :firstArticle, 20";
        $bindings = [
            "firstArticle" => $page*20
        ];
        
        $donnees = $this->queryWithIntegral($request, $bindings);
        
        foreach ($donnees as $properties) {
            $users[] = new User($properties);
        }
        
        return $users;
    }
    
    /**
     * Recupere le nombre de pages
     */
    public function totalPages():int
    {
        $request="SELECT COUNT(us_id) AS nbBillet FROM utilisateurs";
        
        return (int) floor($this->query($request)[0]["nbBillet"]/20);
    }
    
    /**
     * Connexion manuelle via une page de login
     */
    public function connexion(string $login):User
    {
        $request = "SELECT
                us_id AS id,
                us_login AS login,
                us_password AS password,
                us_pseudo AS pseudo,
                us_avatar AS avatar,
                us_email AS email,
                us_admin AS admin
            FROM utilisateurs
            WHERE us_login=?";
        $settings = [$login];
        
        $donnees=$this->query($request,$settings);
        
        if(empty($donnees)) {
            throw new loginException();
        }
        
        return new User($donnees[0]);
    }
    
    /**
     * Connexion automatique via les cookies
     */
    public function autoConnexion(string $login, string $password):User
    {
        $request = "SELECT
                us_id AS id,
                us_login AS login,
                us_pseudo AS pseudo,
                us_avatar AS avatar,
                us_email AS email,
                us_admin AS admin
            FROM utilisateurs
            WHERE us_login=:login AND us_password=:password";
        $settings = [
            "login" => $login,
            "password" => $password
        ];
        
        $donnees = $this->query($request, $settings);
        
        if (!empty($donnees)) {
            return new User($donnees[0]);
        }
    }
    
    /**
     * Verifient la disponibilité d'une valeur pour un champ dans la table des utilisateurs
     */
    private function inexistant(string $champ, string $valeur):bool
    {
        $request="SELECT us_id FROM utilisateurs WHERE ".$champ."=?";
        $settings = [$valeur];
        
        return empty($this->query($request,$settings));
    }
    
    public function loginDisponible(string $login):bool
    {
        return $this->inexistant("us_login", $login);
    }
    
    public function pseudoDisponible(string $pseudo):bool
    {
        return $this->inexistant("us_pseudo", $pseudo);
    }
    
    public function mailDisponible(string $email):bool
    {
        return $this->inexistant("us_email", $email);
    }
    
    /**
     * Mettent à jour les differentes informations d'un utilisateur
     */
    private function update(string $champ, string $valeur, int $id)
    {
        $request = "UPDATE utilisateurs SET ".$champ."=:valeur WHERE us_id=:id";
        $donnees= [
            "valeur"    =>  $valeur,
            "id"        =>  $id
        ];
        
        $this->manage($request, $donnees);
    }
    
    public function updateLogin(string $login, int $id)
    {
        $this->update("us_login", $login, $id);
    }
    
    public function updatePseudo(string $pseudo, int $id)
    {
        $this->update("us_pseudo", $pseudo, $id);
    }
    
    public function updateEmail(string $email, int $id)
    {
        $this->update("us_email", $email, $id);
    }
    
    public function updatePassword(string $password, int $id)
    {
        $this->update("us_password", $password, $id);
    }
    
    public function updateAvatar(string $avatar, string $temporaryName, int $id)
    {   
        $this->update("us_avatar", $avatar, $id);
        
        $directory= "ressources/avatars/".$avatar;
        
        move_uploaded_file($temporaryName, $directory);
    }
    
    public function updateAdmin(bool $admin, int $id)
    {
        $this->update("us_admin", $admin, $id);
    }
        
    public function delete (int $id) {
        $request = "DELETE FROM utilisateurs WHERE  us_id=?";
        $donnees = [$id];
        
        $this->manage($request, $donnees);

    }
}
