<?php
    namespace app\models;
    
    use app\entities\Edito;
    use core\models\Manager;
    
    class EditosManager extends Manager
    { 
        /**
         * Id du dernier edito
         * @var int
         */
        private $lastId;
        
        public function __construct($database)
        {
            parent::__construct($database);
            $this->setLastId();
        }
        
        public function add(string $titre, string $contenu)
        {
            $request = "INSERT INTO editos( ed_titre, ed_contenu, ed_date_ajout) VALUES (:titre, :contenu, NOW())";
            $donnees = ["titre" => $titre, "contenu" => $contenu];
            
            $this->manage($request, $donnees);
        }
        
        public function one(int $id):Edito
        {
            $request="SELECT
                        ed_id AS id,
                        ed_titre AS titre,
                        ed_contenu AS contenu,
                        DATE_FORMAT(ed_date_ajout,'Le %d/%m/%Y à %H:%i') AS dateAjout,
                        DATE_FORMAT(ed_date_modif,'(modifié le %d/%m/%Y à %H:%i)') AS dateModif
                     FROM editos WHERE ed_id = ?";
            $settings=[$id];
            
            $donnees = $this->query($request,$settings);
            
            if(!empty($donnees)) {
                return new Edito($donnees[0]);
            } else {
                throw new \Exception("erreur");
            }
        }
        
        public function last():Edito
        {            
            return $this->one($this->lastId);
        }
        
        /**
         * Recuperation par 20 pour paginer la liste des editos dans l'interface de gestion
         */
         public function paginated(int $page):array
        {
            $editos = [];
            $request ="SELECT
                            ed_id AS id,
                            ed_titre AS titre,
                            DATE_FORMAT(ed_date_ajout,'%d/%m/%Y') AS dateAjout
                        FROM editos
                        ORDER BY ed_id DESC
                        LIMIT :firstArticle, 20";
            $bindings = [
                "firstArticle" => $page*20
            ];
            
            $donnees = $this->queryWithIntegral($request, $bindings);
            
            foreach ($donnees as $properties) {
                $editos[] = new Edito($properties);
            }
            
            return $editos;
        }
        
        /**
         * Recupere le nombre de pages
         */
        public function totalPages():int
        {
            $request="SELECT COUNT(ed_id) AS nbBillet FROM editos";
            
            return (int) floor($this->query($request)[0]["nbBillet"]/20);
        }
    
        public function update(string $titre, string $contenu, int $id)
        {
            $request = "UPDATE editos
                        SET ed_titre= :titre,
                            ed_contenu= :contenu,
                            ed_date_modif=NOW()
                        WHERE ed_id= :id";
            $donnees = [
                "titre"   =>  $titre,
                "contenu" =>  $contenu,
                "id"      =>  $id
            ];
            
            $this->manage($request, $donnees);
        }
    
        public function delete(int $id)
        {
            $request = "DELETE FROM editos WHERE ed_id=?";
            $donnees = [$id];
            $this->manage($request, $donnees);
        }
        
        /**
         *  GETTERS & SETTERS
         */
        
        public function getLastId():int
        {
            return $this->lastId;
        }
        
        private function setLastId()
        {
            $request = "SELECT MAX(ed_id) AS id FROM editos";
            $reponse = $this->query($request);
            $this->lastId = $reponse[0]["id"];
        }
    }
