<?php
    namespace app\views\frontend;
    
    use app\views\elements\Misdirection;
                
    class Page404 extends FrontView
    {
        use Misdirection {
            setContenu as protected hawaii;
        }
        
        protected function setContenu(string $url="index.php")
        {
            $this->hawaii($url);
        }
    }
