<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\UserView;
    
    class NewUserView extends UserView
    {
        protected function setTitre()
        {
            $this->titre = "Nouvel Utilisateur";
        }
        
        protected function setContenu()
        {
            $this->contenu = $this->userForm();
        }
    }
