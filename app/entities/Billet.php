<?php
    namespace app\entities;
    
    class Billet extends Poste
    {
        /**
         * Est-ce le premier billet ?
         * @var bool
         */
        private $first;
        
        /**
         * Id du premier billet
         * @var int
         */
        private $firstId;
        
        /**
         * Est-ce le dernier billet ?
         * @var bool
         */
        private $last;
        
        /**Id du dernier billet
         * @var int
         */
        private $lastId;
        
        /**
         * Version raccourcis du contenu du billet.
         * @var string
         */
        private $preview;
        
        public function __construct(array $donnees, int $firstId, int $lastId)
        {
            parent::__construct($donnees);
            $this->setFirstId($firstId);
            $this->setLastId($lastId);
            $this->setFirst();
            $this->setLast();
            $this->setPreview();
        }
        
        /**
         *  GETTERS
         */
        
        public function isFirst():bool
        {
            return $this->first;
        }
        
        public function isLast():bool
        {
            return $this->last;
        }
        
        public function getFirstId():int
        {
            return $this->firstId;
        }
    
        public function getLastId():int
        {
            return $this->lastId;
        }
    
        public function getPreview():string
        {
            return $this->preview;
        }
        
        /**
         *  SETTERS
         */
        
        private function setFirstId(int $firstId)
        {
            $this->firstId = $firstId;
        }
        
        private function setLastId(int $lastId)
        {
            $this->lastId = $lastId;
        }
        
        private function setFirst()
        {
            $this->first = $this->id === $this->firstId;
        }
    
        private function setLast()
        {
            $this->last = $this->id === $this->lastId;
        }
        
        private function setPreview($preview = null)
        {
            if (is_null($preview)) {
                $length = (strlen($this->contenu)<500)?strlen($this->contenu):500;
                $this->preview = substr($this->contenu, 0, $length);
            } else {
                $this->preview = $preview;
            }
        }
    }
