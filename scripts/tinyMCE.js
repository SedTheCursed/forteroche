tinymce.init({
	selector:'textarea',
	plugins: "emoticons link",
	toolbar: "undo redo | " +
			"bold italic underline | " +
			"alignleft aligncenter alignright alignjustify | " +
			"bullist numlist | outdent indent | blockquote link | " +
			"emoticons",
	language_url: "scripts/langs/fr_FR.js",
	statusbar:false,
	menubar:false,
	browser_spellcheck: true,
	auto_focus: "mce_14"
});
