<?php
    namespace app\views\frontend;
    
    use app\views\elements\Login;
                
    class LoginView extends FrontView
    {
        use Login;
    }
