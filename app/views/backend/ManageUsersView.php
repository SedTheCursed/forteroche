<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\ManageView;
    
    class ManageUsersView extends ManageView
    {
        protected function commandes(string $items = "Utilisateurs"):string
        {
            return parent::commandes($items);
        }
        
        protected function setTitre()
        {
            $this->titre = "Utilisateurs";
        }
        
        protected function setContenu()
        {
            $titre = "Utilisateurs";
            $head = ["","Pseudo"];
            $entity = "User";
            
            ob_start();
            ?>
            <h1><?= $titre ?></h1>
            <table>
            	<thead>
                	<tr>
                        <?php foreach($head as $column): ?>
                        <th><?= $column;?></th>
                        <?php endforeach; ?>
                        <th colspan="2"></th>
                    </tr>
            	</thead>
                <tbody>
                	<?php foreach($this->items as $item):?>
                	<tr>
                		<td><img class="avatar" alt="avatar de <?= $item->getPseudo();?>" src="<?= $item->getAvatar(); ?>">
                		<td><?= $item->getPseudo(); ?></td>
                        <td><a href="?page=modification<?= $entity; ?>&amp;id=<?= $item->getId(); ?>">Modifier</a></td>
                        <td>
                        	<?php if (!$item->getAdmin()):?>
                        	<a href="?page=delete<?= $entity; ?>&amp;id=<?= $item->getId(); ?>">Supprimer</a>
                        	<?php endif; ?>
                		</td>
                    </tr>
                	<?php endforeach; ?>
                </tbody>
			</table>
			<?= $this->commandes($entity."s"); ?>
            <?php
            $this->contenu = ob_get_clean();
        }
    }
