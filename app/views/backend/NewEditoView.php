<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\TextView;
    
    class NewEditoView extends TextView
    {
        
        protected function setTitre()
        {
            $this->titre = "Nouvel Edito";
        }
        
        protected function setContenu()
        {
            $this->contenu = $this->textForm("Edito");
        }
    }
