<?php
    namespace app\controllers;
    
    use core\models\Manager;
    
    abstract class Controller
    {
        /**
         * @var Manager
         */
        protected $manager;
        
        /**
         * Adresse de la page où retourner après la connexion
         * @var string
         */
        protected $retour;
        
        /**
         * Determine l'action du controller
         * @var string
         */
        protected $action;
        
        /**
         * Id de l'entité à contrôler
         * @var int
         */
        protected $id;
        
        /**
         * Commandes à appliquer en cas de mise-à-jour de l'entité
         * @var string
         */
        protected $updateCommands;
        
        public function __construct(Manager $manager, array $donnees, string $action, string $retour)
        {
            $this->setManager($manager);
            $this->setRetour($retour);
            $this->setAction($action);

            if (isset($donnees["id"])) {
                $this->setId($donnees["id"]);
            }
            
            $this->hydrate($donnees);
            if ($this->action === "update") {
                $this->setUpdateCommands();
            }
        }
        
        protected function hydrate(array $donnees)
        {
            foreach($donnees as $key => $value) {
                $method = "set".ucfirst($key);
                
                if (method_exists($this, $method)) {
                    try {
                        $this->$method($value);
                    } catch (\Exception $e) {
                        header("location:".$this->retour."&erreur=".$e->getMessage());
                    }
                }
            }
        }
        
        /**
         * Procède à l'ajout d'une entité
         */
        abstract protected function add();
        
        /**
         * Prcède à la mise à jour d'une entité
         */
        protected function update()
        {    
            try {
                $url = $this->retour."&success=update";
                $this->updateCommands;
                header("location:".$url);
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage());
            }
        }
        
        /**
         * Procède à la suppression d'une entité 
         */
        abstract protected function delete();
        
        /**
         * Execute le processus de contrôle de l'entité
         */
        public function exec()
        {
            switch($this->action) {
                case "add":
                    $this->add();
                    break;
                    
                case "update":
                    $this->update();
                    break;
                    
                case "delete":
                    $this->delete();
                    break;
            }
        }
        
        /**
         *  SETTERS
         */
        
        protected function setManager(Manager $manager)
        {
            $this->manager = $manager;
        }
    
        protected function setRetour(string $retour)
        {
            $this->retour = $retour;
        }
            
        protected function setAction(string $action)
        {
            $this->action = $action;
        }
        
        protected function setId(int $id)
        {
            if($id>0) {
                $this->id = $id;
            }
        }
        
        abstract protected function setUpdateCommands();
    }
