<?php
    namespace app\views\frontend;
    
    class ArchivesView extends FrontView
    {
        /**
         * Ensemble de tous les billets
         * @var array
         */
        private $archives;
        
        public function __construct(array $archives)
        {
            $this->setArchives($archives);
            parent::__construct();
        }
        
        private function setArchives(array $archives)
        {
            $this->archives = $archives;
        }
        
        protected function setTitre()
        {
            $this->titre = "Archives";
        }
        
        /**
         * Affiche en colonne de 10 tous les billets
         */
        protected function setContenu()
        {
            ob_start();
            ?>
            <h1>Archives</h1>
            <div class="archives">
            <?php
            $nbBillets = count($this->archives);
            for ($i=0; $i < $nbBillets;$i=$i+10):?>
        		<ul>
            <?php for ($j=$i; $j<count($this->archives) and $j<$i+10; $j++): ?>
            		<li>
            			<?= $this->archives[$j]->getDateAjout(); ?>&nbsp;:
            			<a href="?page=billet&amp;id=<?= $this->archives[$j]->getId()?>">
            				<?= $this->archives[$j]->getTitre(); ?>
            			</a>
            		</li>
            <?php endfor; ?>
            	</ul>
            <?php endfor; ?>
            </div>
            <?php
            $this->contenu = ob_get_clean();           
        }
    }
