<?php
namespace core\exceptions;

class AdminException extends \Exception
{
    protected $message = "Il faut être un administrateur pour accéder à cette section";
}

