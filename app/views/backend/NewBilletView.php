<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\TextView;
    
    class NewBilletView extends TextView
    {   
        protected function setTitre()
        {
            $this->titre = "Nouveau Billet";
        }
        
        protected function setContenu()
        {
            $this->contenu = $this->textForm("Billet");
        }
    }
