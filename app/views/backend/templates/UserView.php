<?php
    namespace app\views\backend\templates;
    
    use core\view\Form;
                
    abstract class UserView extends BackView
    {
        use Form;
        
        /**
         * Création d'une formulaire d'ajout ou de modification d'utilisateur
         * Le premier paramètre determine si c'est un formulaire d'ajout, les suivants étant les valeurs actuelles de l'utilisateur
         */
        protected function userForm(
            bool $new = true,
            string $login = null,
            string $pseudo = null,
            string $email = null,
            string $avatar = null,
            bool $admin = null): string
        {
            $action = ($new)? "addUser": "updateUser";
            $pageTitle = ($new)? "Nouvel Utilisateur" : "Modifier";
            
            ob_start();
            ?>
                <form action="?page=<?=$action; ?>" method="post" enctype="multipart/form-data">
                	<h1><?= $pageTitle; ?></h1>
                	<?php if (isset($_GET["erreur"])): ?>
                    <p class="alert"><?= ucfirst($_GET["erreur"]); ?></p>
                	<?php elseif (isset($_GET["success"])): ?>
    				<p class='success'>L'Utilisateur a bien été <?= ($new)?"ajouté.":"mis à jour.";?></p>
                	<?php endif; ?>
                	
                    <?php
                    	if ($new) {
                    	    echo $this->champ("login", "Login", true);
                    	    echo $this->champ("pseudo", "Pseudo", false);
                    	    echo $this->champ("email", "Email", false, "email");
                        } else {
                            echo $this->champ("login", "Login", false, "text", $login);
                            echo $this->champ("pseudo", "Pseudo", false, "text", $pseudo);
                            echo $this->champ("email", "Email", false, "email", $email);
                        }
                    ?>
					<p id="pwdDissemblables">Les mots de passe ne correspondent pas.</p>
                    <div class="form-group">
                    	<label for="password">Mot de Passe&nbsp;:</label>
                    	<input type="password" class="form-control" name="password" id="password" <?= ($new)?"required":""; ?> />
                    </div>
                    <div class="form-group">
                    	<label for="confirmation">Confirmer le mot de passe&nbsp;:</label>
                    	<input type="password" class="form-control" id="confirmation" <?= ($new)?"required":""; ?> />
                    </div>
                      
                    <?php if (!$new) :?>
                    <div class="form-group">
                    	<label>Avatar&nbsp;:</label>
                    	<?php if ($avatar !== null) :?>
        				<img class="avatar" src="<?= $avatar;?>" alt="avatar de <?= $pseudo; ?>"/>
        				<?php endif;?>
        				<input type="file" name="avatar" id="avatar" />
                    </div>
                    <?php endif;?>
                    <div class="radiochoice">
                    	<strong>Administrateur&nbsp;:</strong>
                    	<?php if ($admin !== null) : ?>
                    	<div class="radio-inline"><label><input type="radio" name="admin" value="oui" <?= ($admin)?"checked":""; ?>/>Oui</label></div>
                    	<div class="radio-inline"><label><input type="radio" name="admin" value="non" <?= (!$admin)?"checked":""; ?>/>Non</label></div>
                    	<?php else : ?>
                    	<div class="radio-inline"><label><input type="radio" name="admin" value="oui" />Oui</label></div>
                    	<div class="radio-inline"><label><input type="radio" name="admin" value="non" checked />Non</label></div>
                    	<?php endif; ?>
                    </div>
                	<p>
                	<?php if (!$new && isset($_GET["id"])) : ?>
                		<input type="hidden" name="id" value="<?= $_GET["id"]; ?>" />
            		<?php endif; ?>
                        <input type="submit" value="<?= ($new)?"Ajouter":"Modifier"; ?>"/>
                        <input type="reset" value="Annuler" />
        			</p>
                </form>
                <?php
                return ob_get_clean();
            }
            
            protected function setScript()
            {
                $this->script = "<script src='scripts/inscription.js'></script>";
            }
    }
