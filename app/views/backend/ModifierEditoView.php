<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\ModifierView;
    use app\entities\Edito;
    
    class ModifierEditoView extends ModifierView
    {
        public function __construct(Edito $edito, string $retour)
        {
            parent::__construct($edito, $retour);
        }
        
        protected function setTitre()
        {
            $this->titre = "Modifier ".$this->item->getTitre();
        }
        
        protected function setContenu()
        {
            $titre = $this->item->getTitre();
            $contenu = $this->item->getContenu();
            
            ob_start();
            echo $this->textForm("Edito", true, false, $titre, $contenu);
            ?>
           	<div><a href="<?= $this->retour; ?>"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a></div>
            <?php
            $this->contenu = ob_get_clean();
        }
    }
