<?php
    namespace app\views\backend;
    
    use app\views\backend\templates\ManageView;
                
    class ManageEditosView extends ManageView
    {
        protected function commandes(string $items = "Editos"):string
        {
            return parent::commandes($items);
        }
        
        protected function setTitre()
        {
            $this->titre = "Editos";
        }
        
        protected function setContenu()
        {
            $titre = "Editos";
            $head = ["Date","Titre"];
            $body = ["getDateAjout","getTitre"];
            $entity = "Edito";
            
            $this->contenu = $this->affichage($titre, $head, $body, $entity);
        }
    }
