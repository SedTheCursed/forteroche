<?php
    namespace core\models;
    
    use \core\exceptions\DBException;
                
    class MySqlDB
    {
        /**
         * La base de données
         * @var \PDO
         */
        private $db;
        
        /**
         * Tableau contenant les informations pour la connexion à la base de données
         * @var array
         */
        private $settings;
        
        /**
         * Instance unique pour faire de la classe MySqlDb un singleton
         * @var MySqlDB
         */
        private static $instance;
        
        private function __construct()
        {
            $this->settings = require 'config/config.php';
        }
        
        public function getDb():\PDO
        {          
            if(!isset($this->db)){
                try {
                    $this->db = new \PDO("mysql:host=".$this->settings["db_host"]."; dbname=".$this->settings["db_name"]."; charset=utf8",
                        $this->settings["db_user"],
                        $this->settings["db_password"],
                        [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]);
                } catch (\Exception $e) {
                    throw new DBException();
                }
            }
            
            return $this->db;
        }
        
        public static function getInstance():MySqlDB
        {
            if (!isset(self::$instance)){
                self::$instance = new self;
            }
            return self::$instance;
        }
    }
