<?php
    namespace app\entities;
    
    use core\entities\Entity;
    
    class User extends Entity
    {
        /**
         * Id unique de l'utilisateur
         * @var int
         */
        private $id;
        
        /**
         * Identifiant de connexion de l'utilisateur
         * @var string
         */
        private $login;
        
        /**
         * Mot de passe de l'utilisateur
         * @var string
         */
        private $password;
        
        /**
         * Nom utilisé sur le site par l'utilisateur
         * @var string
         */
        private $pseudo;
        
        /**
         * Nom du fichier servant de photo à l'utilisateur
         * @var string
         */
        private $avatar;
        
        /**
         * E-mail de l'utilisateur
         * @var string
         */
        private $email;
        
        /**
         * Statut d'administrateur de l'utilisateur
         * @var bool
         */
        private $admin;
        
        /**
         *  GETTERS
         */
        
        public function getId():int
        {
            return $this->id;
        }
    
        public function getLogin():string
        {
            return $this->login;
        }
    
        public function getPassword():string
        {
            return $this->password;
        }
    
        public function getPseudo():string
        {
            return $this->pseudo;
        }
    
        public function getAvatar():string
        {
            return $this->avatar;
        }
    
        public function getEmail():string
        {
            return $this->email;
        }
    
        public function getAdmin():bool
        {
            return $this->admin;
        }
    
        /**
         *  SETTERS
         */
        
        public function setId(int $id)
        {
            $this->id = $id;
        }
        
        public function setLogin(string $login)
        {
            $this->login = $login;
        }
        
        public function setPassword(string $password)
        {
            $this->password = $password;
        }
    
        public function setPseudo(string $pseudo)
        {
            if (empty($pseudo)) {
                $this->pseudo = ucfirst($this->login);
            } else {
                $this->pseudo = $pseudo;
            }
        }
    
        public function setAvatar($avatar)
        {
            if(!is_null($avatar)) {
                $this->avatar = "ressources/avatars/".$avatar;
            } else {
                $this->avatar = "ressources/avatars/inconnu.png";
            }
        }
    
        public function setEmail(string $email)
        {
            $this->email = $email;
        }
    
        public function setAdmin($role)
        {
            if (!is_null($role) && $role) {
                $this->admin = true;
            } else {
                $this->admin = false;
            }
        }    
    }
