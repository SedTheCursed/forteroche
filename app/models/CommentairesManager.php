<?php
    namespace app\models;
    
    use core\models\Manager;
    use app\entities\Commentaire;
                    
    class CommentairesManager extends Manager
    {
        /**
         * Id du dernier commentaire
         * @var int
         */
        private $lastId;
        
        public function __construct(\PDO $database)
        {
            parent::__construct($database);
            $this->setLastId();
        }
        
        public function create(string $contenu, $auteur, int $article, $reponse = null)
        {
            $request = "INSERT INTO commentaires(com_contenu, com_auteur, com_date_ajout, com_article, com_reponse)
                            VALUES (:contenu, :auteur, NOW(), :article, :reponse)";
            $donnees = [
                "contenu" => $contenu,
                "auteur" => $auteur,
                "article" => $article,
                "reponse" => $reponse
            ];
            
            $this->manage($request, $donnees);
        }
        
        public function articleComments(int $articleId):array
        {
            $commentaires = [];
            $request = "SELECT
                            c.com_id AS id,
                            c.com_contenu AS contenu,
                            DATE_FORMAT(c.com_date_ajout,'%d/%m/%Y à %H:%i') AS dateAjout,
                            DATE_FORMAT(c.com_date_modif,'(modifié le %d/%m/%Y à %H:%i)') AS dateModif,
                            c.com_article AS article,
                            c.com_reponse AS reponse,
                            a.us_pseudo AS auteur,
                            a.us_avatar AS avatar
                        FROM commentaires AS c
                        LEFT JOIN utilisateurs AS a
                        ON  c.com_auteur = a.us_id  
                        WHERE com_article = ?
                        ORDER BY com_date_ajout";
            $settings = [$articleId];
            $donnees = $this->query($request, $settings);
            
            foreach ($donnees as $proprietes) {
                $commentaires[] = new Commentaire($proprietes);
            }
            
            return $commentaires;
        }
        
        public function one(int $id):Commentaire
        {
            $request= "SELECT
                            c.com_id AS id,
                            c.com_contenu AS contenu,
                            DATE_FORMAT(c.com_date_ajout,'%d/%m/%Y à %H:%i') AS dateAjout,
                            DATE_FORMAT(c.com_date_modif,'(modifié le %d/%m/%Y à %H:%i)') AS dateModif,
                            c.com_article AS article,
                            a.us_pseudo AS auteur
                        FROM commentaires AS c
                        LEFT JOIN utilisateurs AS a
                        ON c.com_auteur = a.us_id
                        WHERE com_id = ?";
            $settings=[$id];
            
            $donnees = $this->query($request,$settings)[0];
            
            return new Commentaire($donnees);
        }
        
        public function last(): Commentaire
        {
            return $this->one($this->lastId);
        }
        
        /**
         * Recupere tous les commentaires postés par un utilisateur
         */
        public function postedByUser(int $id):array
        {
            $commentaires = [];
            $request = "SELECT com_id AS id FROM commentaires WHERE com_auteur=?";
            $settings = [$id];
            
            $donnees = $this->query($request, $settings);
            
            foreach ($donnees as $commentaire) {
                $commentaires[]=$commentaire["id"];
            }
            
            return $commentaires;
        }
        
        private function hasReponse(int $id):bool
        {
            $request = "SELECT COUNT(com_id) AS nbReponse FROM commentaires WHERE com_reponse=?";
            $donnees = [$id];
            
            $nbReponse = $this->query($request, $donnees)[0]["nbReponse"];
            
            return $nbReponse>0;
        }
        
        private function isReponse (int $id):bool
        {
          $request = "SELECT com_reponse AS reponse FROM commentaires WHERE com_id=?";
          $donnees = [$id];
          
          return !is_null($this->query($request, $donnees)[0]["reponse"]);
        }
        
        /**
         * Recuperation par 20 pour paginer la liste des commentaires dans l'interface de gestion
         */
        public function paginated(int $page):array
        {
            $comments = [];
            $request ="SELECT
                            c.com_id AS id,
                            c.com_contenu AS contenu,
                            DATE_FORMAT(c.com_date_ajout,'%d/%m/%Y') AS dateAjout,
                            b.bill_titre AS articleTitre
                        FROM commentaires AS c
                        LEFT JOIN billets AS b
                        ON c.com_article = b.bill_id
                        ORDER BY c.com_id DESC
                        LIMIT :firstArticle, 20";
            $bindings = [
                "firstArticle" => $page*20
            ];
            
            $donnees = $this->queryWithIntegral($request, $bindings);
            
            foreach ($donnees as $properties) {
                if ($properties["contenu"] !== "<p>&lt;Message Effacé&gt;</p><p></p>"){
                    $comments[] = new Commentaire($properties);
                }
            }

            return $comments;
        }
        
        /**
         * recupère le nombre de page
         */
        public function totalPages():int
        {
            $request="SELECT COUNT(com_id) AS nbBillet FROM commentaires";
            
            return (int) floor($this->query($request)[0]["nbBillet"]/20);
        }
        
        public function updateContent(int $id, string $content)
        {
            $request = "UPDATE commentaires SET com_contenu = :contenu, com_date_modif = NOW() WHERE com_id = :id";
            $donnees = [
                "contenu"   =>  $content,
                "id"        =>  $id
            ];
            
            $this->manage($request, $donnees);
        }
        
        /**
         * Change l'auteur des articles d'un utilisateur supprimé en un utilisateur fantôme 
         */
        public function fromDeletedUser(int $auteur)
        {
            $request = "UPDATE commentaires SET com_auteur = 2 WHERE com_auteur=?";
            $donnees = [$auteur];
            
            $this->manage($request, $donnees);
        }
        
        /**
         * Change le destinaire dans les commentaires repondant à ceux d'un utilisateur supprimés 
         */
        public function toDeletedUser(string $deletedUserName)
        {
            $destinataire = "@".$deletedUserName;
            
            $request = "SELECT com_contenu AS contenu, com_id AS id FROM commentaires WHERE com_contenu REGEXP ?";
            $settings = [$destinataire];
            
            $reponses = $this->query($request, $settings);
            
            foreach ($reponses AS $reponse) {
                $updatedContent = preg_replace("#".$deletedUserName."#", "&lt;UTILISATEUR SUPPRIMÉ&gt;", $reponse["contenu"]);
                
                $this->updateContent($reponse["id"], $updatedContent);
            }
        }
        
        public function delete(int $id)
        {            
            $request = "DELETE FROM commentaires WHERE com_id=?";
            $donnees = [$id];
            
            $this->manage($request, $donnees);
        }
        
        /**
         * Change le texte d'une message demandé à être effacé s'il en ou a une réponse, sinon l'éfface.
         */
        
        public function manualDelete(int $id)
        {
            if ($this->hasReponse($id) | $this->isReponse($id)) {
                $this->updateContent($id, "<p>&lt;Message Effacé&gt;</p><p></p>");
            } else {
               $this->delete($id);
            }
        }
        
        private function setLastId()
        {
            $request = "SELECT MAX(com_id) AS id FROM commentaires";
            $reponse = $this->query($request);
            $this->lastId = $reponse[0]["id"];
        }
    }
