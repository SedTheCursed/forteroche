<?php
namespace app\views\elements;

class NavbarItems
{
    /**
     * Chaque entity correspond à une section du back-office
     * @var array
     */
    private $items = ["Billets", "Edito", "Commentaires", "Utilisateurs"];
    
    /**
     * Créé un menu deroulant dans la barre d'entête pour chaque section du back office 
     */
    public function view():string
    {
        ob_start();
        foreach($this->items as $item) {
        ?>    
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $item; ?></a>
                <ul class="dropdown-menu">
            	<?php if($item !== "Commentaires"): ?>
    				<li><a href="?page=new<?= $item; ?>">Nouveau</a></li>
				<?php endif; ?>
                	<li><a href="?page=manage<?= $item; ?>">Gérer</a></li>
                </ul>
			</li>
		<?php	
        }
        return ob_get_clean();
    }
}
