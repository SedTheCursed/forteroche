$(function(){
	$("a[href^='?page=delete']").each(function(){
		$(this).on("click",function(e){
			$delete = confirm("Voulez-vous vraiment supprimer ceci ?\n(Cette action est irréversible.)");
			
			if (!$delete) {
				e.preventDefault();
			}
		});
	});
});