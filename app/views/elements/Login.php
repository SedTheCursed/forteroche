<?php
    namespace app\views\elements;
    
    use core\view\Form;
    
    trait Login
    {
        use Form;
        
        protected function setTitre()
        {
            $this->titre = "Se connecter";
        }
        
        protected function setContenu()
        {   ob_start();
        ?>
            <h1>Se connecter</h1>
            <form action="?page=connexion" method="post">
            
            <?php if (isset($_GET["erreur"])): ?>
            <p class="alert"><?= ucfirst($_GET["erreur"]); ?></p>
            <?php
            endif;
            
            echo $this->champ("login", "Votre identifiant", true);
            echo $this->champ("password", "Votre mot de passe", false, "password");
            ?>
            	<p><input type="checkbox" name="stayCo" id="stayCo"> <label for="stayCo">Rester connecté.</label></p>
            	<p>
                	<input type="submit" value="Se connecter" />
                	<input type="reset" value="Annuler"/>
            	</p>
            </form>
            <?php
            $this->contenu = ob_get_clean();
        }
    }

