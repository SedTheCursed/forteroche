<?php
    namespace core\models;
                
    abstract class Manager
    {
        /**
         * Base de données avec laquelle le manager interagit.
         * @var \PDO
         */
        protected $db;
        
        public function __construct(\PDO $database)
        {
            $this->setDB($database);
        }
        
        /**
         * methode générique pour la modification de la base de données
         */
        protected function manage(string $request, array $donnees)
        {
            $req = $this->db->prepare($request);
            $req->execute($donnees);
            
            unset($req);
        }
        
        /**
         * Methode générique pour la lecture de la base de données
         */
        protected function query(string $request, array $settings=null):array
        {

            if (is_null($settings)) {
                $req = $this->db->query($request);
            } else {
                $req = $this->db->prepare($request);
                $req->execute($settings);
            }
        
            $donnees=$req->fetchAll(\PDO::FETCH_ASSOC);
            $req->closeCursor();
            
            return $donnees;
        }
        
        /**
         * Methode générique pour la lecture de la base de données quand certains parametres sont des intégraux.
         */
        protected function queryWithIntegral(string $request, array $bindings, array $settings=null):array
        {
            $req = $this->db->prepare($request);
            
            foreach ($bindings as $binding => $value) {
                $req->bindValue($binding, $value, \PDO::PARAM_INT);
            }
            
            if (is_null($settings)) {
                $req->execute();
            } else {
                $req->execute($settings);
            }
            
            $donnees=$req->fetchAll(\PDO::FETCH_ASSOC);
            $req->closeCursor();
            
            return $donnees;
        }
        
        
        /** 
         *  GETTERS & SETTERS 
         */
        public function getDB():\PDO
        {
            return $this->db;
        }
            
        protected function setDB(\PDO $database)
        {
            $this->db = $database;
        }
    }
