<?php
    namespace app\views\elements;
    
    abstract class View
    {
        /**
         * Contenu de la blasie title de la page
         * @var string
         */
        protected $titre;
        
        /**
         * Contenu de la balise main de la page
         * @var string
         */
        protected $contenu;
        
        /**
         * Balises <script> nécessaire au fonctionnement de la page.
         * @var string
         */
        protected $script;
        
        public function __construct()
        {
            $this->setTitre();
            $this->setContenu();
            $this->setScript();
        }
        
        /**
         * Integre les contenu dans le template de page (permettant son affichage)
         */
        protected function view(string $template)
        {
            $page = $this;
            require dirname(__DIR__).$template;
        }
        
        public function getTitre():string
        {
            return $this->titre;
        }
    
        public function getContenu():string
        {
            return $this->contenu;
        }
    
        public function getScript():string
        {
            return $this->script;
        }
    
        abstract protected function setTitre();
        abstract protected function setContenu();
        
        protected function setScript()
        {
            $this->script = "";
        }
    }
