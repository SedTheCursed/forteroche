<?php
    namespace app\controllers;
    
    use core\models\MySqlDB;
                
    abstract class Router
    {
        /**
         * Base de données de l'application
         * @var \PDO
         */
        protected $db;
        
        /**
         * Etat de connexion du visiteur
         * @var bool
         */
        protected $connecte;
        
        /**
         * Page à afficher ou à executer
         * @var \app\views\elements\View
         */
        protected $page;
        
        public function __construct()
        {
            $this->setDb();
            $this->setConnecte();
            $this->setPage();
        }
        
        /**
         * Procede à l'execution de la page
         */
        public function exec()
        {
            if (method_exists($this->page, "view")) {
                $this->page->view();
            } elseif (method_exists($this->page, "exec")) {
                $this->page->exec();
            }
        }
        
        /**
         *  SETTERS
         */
        
        protected function setDb()
        {
            try {
                $this->db = MySqlDB::getInstance()->getDB();
            } catch (\Exception $e) {
                $this->page = new \app\views\frontend\ErrorView($e);
            }
        }
        
        protected function setConnecte()
        {
            //mise en place de la session et connexion par cookies
            session_start();
            if (isset($_COOKIE["login"]) && isset($_COOKIE["password"])) {
                $users=new \app\models\UsersManager($this->db);
                $_SESSION["user"] = $users->autoConnexion($_COOKIE["login"], $_COOKIE["password"]);
            }
            
            $this->connecte = isset($_SESSION["user"]);
        }
        
        abstract protected function setPage();
    }
