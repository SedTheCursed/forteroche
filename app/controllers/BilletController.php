<?php
    namespace app\controllers;
    
    use app\models\BilletsManager;
    use app\models\CommentairesManager;
                    
    class BilletController extends Controller
    {
        /**
         * @var CommentairesManager
         */
        private $commentsManager;
        
        /**
         * @var string
         */
        private $titre;
        
        /**
         * @var string
         */
        private $contenu;
        
        public function __construct(
            BilletsManager $manager,
            array $donnees,
            string $action,
            string $retour,
            CommentairesManager $comments=null)
        {
            $this->setCommentsManager($comments);
            parent::__construct($manager, $donnees, $action, $retour);
        }

        /**
         * @see \app\controllers\Controller::add()
         */
        protected function add()
        {
            try {
                $url = $this->retour;
                $url .= (preg_match("#admin.php$#",$this->retour))?"?":"&";
                $url .= "success=ajout";
                
                $this->manager->create($this->titre, $this->contenu);
                header("location:".$url);
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage());
            }
        }
        
        /**
         * @see \app\controllers\Controller::delete()
         * Efface l'article et les commentaires liés
         */
        protected function delete()
        {
            try {
                $this->manager->delete($this->id);
                $comments = $this->commentsManager->articleComments($this->id);
                
                foreach ($comments as $comment) {
                    $this->commentsManager->delete($comment->getId());
                }
                
                header("location:".$this->retour);
                
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage()); 
            }
        }
        
        /**
         *  SETTERS
         */
        
        /**
         * @param CommentairesManager $comments
         */
        protected function setCommentsManager($comments)
        {
            $this->commentsManager = $comments;
        }
        
        protected function setTitre(string $titre)
        {
            $this->titre = $titre;
        }
        
        protected function setContenu(string $contenu)
        {
            $this->contenu = $contenu;
        }
        
        /**
         * @see \app\controllers\Controller::setUpdateCommands()
         */
        protected function setUpdateCommands()
        {
            ob_start();
            $this->manager->update($this->titre, $this->contenu, $this->id);
            $this->updateCommands = ob_get_clean();
        }
    }
