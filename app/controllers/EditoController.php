<?php
    namespace app\controllers;
    
    use app\models\EditosManager;
    
    class EditoController extends Controller
    {
        /**
         * Titre de l'edito
         * @var string
         */
        private $titre;
        
        
        /**
         * Contenu de l'edito
         * @var string
         */
        private $contenu;
        
        public function __construct(EditosManager $manager, array $donnees, string $action, string $retour)
        {
            parent::__construct($manager, $donnees, $action, $retour);
        }
        
        /**
         * @see \app\controllers\Controller::add()
         */
        protected function add(){            
            try {
                $url = $this->retour."&success=add";
                
                $this->manager->add($this->titre, $this->contenu);
                header("location:".$url);
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage());
            }
        }
        
        /**
         * @see \app\controllers\Controller::delete()
         */
        protected function delete(){
            try {
                $this->manager->delete($this->id);
                header("location:".$this->retour);  
            } catch (\Exception $e) {
                header("location:".$this->retour."&erreur=".$e->getMessage());
            }
        }
        
        /**
         *  SETTERS
         */
        
        protected function setTitre($titre)
        {
            $this->titre = $titre;
        }
    
        protected function setContenu($contenu)
        {
            $this->contenu = $contenu;
        }
        
        protected function setUpdateCommands() {
            ob_start();
            $this->manager->update($this->titre, $this->contenu, $this->id);
            $this->updateCommands = ob_get_clean();
        }
    }
