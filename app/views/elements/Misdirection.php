<?php
namespace app\views\elements;

trait Misdirection 
{
    protected function setTitre()
    {
        $this->titre = "Bienvenue à Hawaï";
        
    }
    
    protected function setContenu(string $url)
    {
        ob_start();
        ?>
            <h1>
            	Aloha<br/>et<br/>Bienvenue à Hawaï
            </h1>
            <p class="flag"><img  src="img/Hawaii.png" alt="drapeau de Hawaï"><p>
            <p class="page404">
            	Vous aviez <strong>un billet simple pour l'Alaska</strong>&nbsp;?<br/>
            	Vous avez dû vous égarer quelque part, c'est assez ennuyeux.<br/>
            	Nous ne pouvons que vous conseiller que de retourner à <a href="<?= $url; ?>">l'accueil</a> afin de remettre votre voyage sur le bon chemin.<br/>
            	Aloha !
            </p>
            <?php
            $this->contenu = ob_get_clean();
        }
}

