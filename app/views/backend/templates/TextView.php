<?php
    namespace app\views\backend\templates;
    
    use core\view\Form;
    
    abstract class TextView extends BackView
    {
        use Form;
        
        /**
         * Création d'un formulaire d'ajout ou de modification d'un texte 
         * @param string $item : le type de texte
         * @param bool $mainContent : savoir si le formulaire est l'element central de la page
         * @param bool $new : Est-ce un formulaire d'ajout ?
         * @param string $titre : Valeur du titre (en cas de modification)
         * @param string $contenu : Valeur du contenu (en cas de modification)
         * @param bool $hasTitre : le texte as-t-il un titre
         * @return string
         */
        protected function textForm(
            string $item,
            bool $mainContent = true,
            bool $new = true,
            string $titre = null,
            string $contenu= null,
            bool $hasTitre = true):string
        {
            $action = ($new)?"add".$item:"update".$item;
            $voyelleInitiale = preg_match("#^[AEIOUYH]#",$item);
            
            if ($new) {
                if ($voyelleInitiale) {
                    $pageTitle = "Nouvel ".$item;
                } else {
                    $pageTitle = "Nouveau ".$item;
                }
            } else {
                $pageTitle = "Modifier";
            }
            
            if ($voyelleInitiale) {
                $item = "L'".$item;
            } else {
                $item = "Le ".$item;
            }
            
            
            ob_start();
            ?>
            <form action="?page=<?=$action; ?>" method="post">
            	<?= ($mainContent) ?"<h1>":"<h2>" ; ?>
            	<?= $pageTitle; ?>
            	<?= ($mainContent) ?"</h1>":"</h2>" ; ?>
            	<?php if (isset($_GET["erreur"])): ?>
                <p class="alert"><?= ucfirst($_GET["erreur"]); ?></p>
            	<?php elseif (isset($_GET["success"])): ?>
				<p class='success'><?= $item; ?> a bien été <?= ($new)?"ajouté.":"mis à jour.";?></p>
            	<?php endif; ?>
            	
            	<?php
                    if ($hasTitre) {
                    	if ($new) {
                    	    echo $this->champ("titre", "Titre", true);
                        } else {
                            echo $this->champ("titre", "Titre", false, "text", $titre);
                        }
                    }
                ?>
            	<textarea name="contenu"><?= ($contenu !== null)?$contenu:""; ?></textarea>
            	<p>
            	<?php if (!$new && isset($_GET["id"])) : ?>
            		<input type="hidden" name="id" value="<?= $_GET["id"]; ?>" />
        		<?php endif; ?>
                    <input type="submit" value="<?= ($new)?"Publier":"Modifier"; ?>"/>
                    <input type="reset" value="Annuler" />
    			</p>
            </form>
            <?php
            return ob_get_clean();
        }
            
        protected function setScript()
        {
            $this->script = "<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=lll7oi734hwr9btz7jcf2to0bavorp8gt2okmvt04rrgro7h'></script>
                <script src='scripts/tinyMCE.js'></script>";
        }
    }
