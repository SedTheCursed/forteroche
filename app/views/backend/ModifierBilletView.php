<?php
    namespace app\views\backend;
    
    use app\entities\Billet;
    use app\views\backend\templates\ModifierView;
                    
    class ModifierBilletView extends ModifierView
    {
        public function __construct(Billet $billet, string $retour)
        {
            parent::__construct($billet, $retour);
        }
        
        protected function setTitre()
        {
            $this->titre = "Modifier ".$this->item->getTitre();
        }
        
        protected function setContenu()
        {
            $titre = $this->item->getTitre();
            $contenu = $this->item->getContenu();
            
            ob_start();
            echo $this->textForm("Billet", true, false, $titre, $contenu);
            ?>
            <div><a href="<?= $this->retour; ?>"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a></div>
            <?php
            $this->contenu = ob_get_clean();
        }
    }
