<?php    
    require 'core/Autoloader.php';
    core\Autoloader::autoload();
    
    $app = new app\controllers\Front();
    $app->exec();
