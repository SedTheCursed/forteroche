<?php
namespace app\views\backend;

use app\views\backend\templates\UserView;
use app\entities\User;

class ModifierUserView extends UserView
{
    /**
     * Utilisateru à modifier
     * @var User
     */
    protected $user;
    /**
     * url du bouton de retour
     * @var string
     */
    protected $retour;
    
    public function __construct(User $user, string $retour)
    {
        $this->setUser($user);
        $this->setRetour($retour);
        parent::__construct();
    }
    
    protected function setTitre()
    {
        $this->titre = "Modifier ".$this->user->getPseudo();
    }
    
    protected function setContenu()
    {
        $login = $this->user->getLogin();
        $pseudo = $this->user->getPseudo();
        $email = $this->user->getEmail();
        $avatar = $this->user->getAvatar();
        $admin = $this->user->getAdmin();
        
        ob_start();
        echo $this->userForm(false, $login, $pseudo, $email, $avatar, $admin);
        ?>
        <div><a href="<?= $this->retour; ?>"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a></div>
        <?php
        $this->contenu = ob_get_clean();
    }

    protected function setUser(User $user)
    {
        $this->user = $user;
    }
    
    protected function setRetour(string $retour)
    {
        $this->retour = $retour;
    }
}

