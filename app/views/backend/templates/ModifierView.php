<?php
    namespace app\views\backend\templates;
    
    use core\entities\Entity;
                
    abstract class ModifierView extends TextView
    {
        /**
         * Entité à modifier
         * @var Entity
         */
        protected $item;
        
        /**
         * Adresse du bouton de retour
         * @var string
         */
        protected $retour;
        
        public function __construct(Entity $item, string $retour)
        {
            $this->setItem($item);
            $this->setRetour($retour);
            parent::__construct();
        }
        
        protected function setItem(Entity $item)
        {
            $this->item = $item;
        }
        
        protected function setRetour(string $retour)
        {
            $this->retour = $retour;
        }
    }
